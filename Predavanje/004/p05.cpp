/*
predavanja: 15-10-2020. cetvrtak

   ponavljanje: 
      - pokazivac na pokazivac
      - agregacija, kompozicija, kreiranje i unistavanje objekata
      - rad za datotekama
      - tokenizacija
    novo:
      - biblioteka FLTK GUI
      - makefile
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;

// linija = "dragan,markovic,1234,2010"
// separator = ","
vector<string> *tokenizacija(string &linija, string &separator){
    vector<string> *ret = new vector<string>();
    while(true){
        int pos = linija.find(separator);
        if(pos == -1)
            break;
        string deo = linija.substr(0, pos);
        ret->push_back(deo);
        linija = linija.substr(pos+separator.length(), linija.length());
    }
    ret->push_back(linija);    
    return ret;
}

class Radnik;
class Projekat;


class Projekat{
public:
   string id;
   string naziv;
   vector<Radnik*> radnici;
   Projekat(string linija){
       string separator = ",";
       vector<string> *r = tokenizacija(linija, separator);
       this->id = (*r)[0];
       this->naziv = (*r)[1];
   }
};

class Radnik {
public:
   string id;
   string ime;
   string prezime;
   double plata;
   int godina_rodjenja;
   string projekat_id;
   Projekat *projekat;
   Radnik(string linija){
       string separator = ",";
       vector<string> *r = tokenizacija(linija, separator);
       // ovde bi bilo dobro proveriti duzinu vektora r
       this->id = (*r)[0];
       this->ime = (*r)[1];
       this->prezime = (*r)[2];
       this->plata = stod((*r)[3]);
       this->godina_rodjenja = stoi((*r)[4]);
       this->projekat_id = (*r)[5];
   }
};


// template <typename T> 
// T myMax(T x, T y) 
// { 
//    return (x > y)? x: y; 
// } 

template <typename T>
vector<T> ucitaj_podatke(string naziv_datoteke){
    vector<T> ret; 
    ifstream ifs(naziv_datoteke);
    string linija;
    while(!ifs.eof()){
        getline(ifs, linija);
        // cout << linija << endl;
        T r(linija);
        ret.push_back(r);
    }
    ifs.close();
    return ret;    
}

// vector<Projekat> ucitaj_projekte(string naziv_datoteke){
//     vector<Projekat> projekti; 
//     ifstream ifs(naziv_datoteke);
//     string linija;
//     while(!ifs.eof()){
//         getline(ifs, linija);
//         // cout << linija << endl;
//         Projekat r(linija);
//         projekti.push_back(r);
//     }
//     ifs.close();
//     return projekti;    
// }

// vector<Radnik> ucitaj_radnike(string naziv_datoteke){
//     vector<Radnik> radnici; 
//     ifstream ifs(naziv_datoteke);
//     string linija;
//     while(!ifs.eof()){
//         getline(ifs, linija);
//         // cout << linija << endl;
//         Radnik r(linija);
//         radnici.push_back(r);
//     }
//     ifs.close();
//     return radnici;
// }


int main(){
    cout << "Dobro jutro 15-10-2020" << endl;

    // test za funkciju tokenizacija
    // string test1 = "aaa, bb##cc,c##123##56,7";
    // string separator = "##";
    // vector<string> *r = tokenizacija(test1, separator);
    // for(string a: *r){
    //     cout << a << endl;
    // }

    vector<Radnik> radnici = ucitaj_podatke<Radnik>("radnici.csv"); 

    cout << "------- radnici -------" << endl;
    for(Radnik r: radnici){
        cout << r.ime << "  " << r.projekat_id << endl;
    }
    
    // ----------  ucitati vektor projekata -------
    vector<Projekat> projekti = ucitaj_podatke<Projekat>("projekti.csv");

    cout << "------- projekti -------" << endl;
    for(Projekat r: projekti){
        cout << r.id << "  " << r.naziv << endl;
    }

    // povezivanje
    // pauza do 11:05



    return 0;
}