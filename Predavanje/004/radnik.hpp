#ifndef RADNIK_HPP
#define RADNIK_HPP

#include <iostream>
#include <string>
#include <vector>
#include "util.h"

using namespace std;


class Radnik{
    public:
        string ime;
        string prezime;
        double plata;
        int godina_rodjenja;
        Radnik(string &linija){
            // linija = "dusan,kurdulija,1500"
            vector<string> delovi = tokenizacija(linija, ",");
            if(delovi.size()==4){
                this->ime = delovi[0];
                this->prezime = delovi[1];
                this->plata = stod(delovi[2]);
                this->godina_rodjenja = stoi(delovi[3]);
            }
        }

        string to_string(){
            string t;
            t = this->ime+","+this->prezime+","+std::to_string(this->plata)+","+std::to_string(this->godina_rodjenja);
            return t;
        }

};
#endif