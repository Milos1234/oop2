#ifndef UTIL
#define UTIL

#include <iostream>
#include <string>
#include <vector>

using namespace std;

vector<string> tokenizacija(string &linija, string separator){
    cout << "--------------------------" << endl;

    vector<string> delovi;
    int pos;
    while(true){
        pos = linija.find(separator);
        if(pos == -1){
            break;
        }
        string deo = linija.substr(0, pos);
        cout << deo << endl;
        delovi.push_back(deo);
        linija = linija.substr(pos+1, linija.length());
    }
    delovi.push_back(linija);
    return delovi;
}

#endif