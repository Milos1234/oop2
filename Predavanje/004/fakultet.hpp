#include <string>
#include "util.h"

using namespace std;

class Fakultet{
    public:
    Fakultet(string &linija){
        // linija == "Tehnicki fakultet,Novi Sad"
        vector<string> delovi = tokenizacija(linija, ",");
        if(delovi.size()==2){
            this->naziv = delovi[0];
            this->mesto = delovi[1];
        }
        // size_t pos = linija.find(",");
        // this->naziv = linija.substr(0, pos);
        // this->mesto = linija.substr(pos+1, linija.length());
    }
    string naziv;
    string mesto;
    string to_string(){
        string t;
        t = this->naziv+","+this->mesto;
        return t;
    }
};
