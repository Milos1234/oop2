/*
Predavanja: 14-10-2020

ponavljanje:
  - varijable, tipovi, nizovi
  - pokazivaci i reference
  - funkcije
  - klase, nasledjivanje, virtual
novo:
  - redefinisanje operatora
  - rad sa txt datotekama
  - tokenizacija stringa
  
  - serijalizacija/deserijalizacija

*/
#include <iostream>
#include  <fstream>
#include <string>
#include <vector>
#include "fakultet.hpp"
#include "radnik.hpp"


using namespace std;


ifstream& operator>>(ifstream& in, string& linija){
    getline(in, linija);
    return in;
}

int main(){
    cout << "Dobro jutro" << endl;

    // ofstream ofs("radnici.csv");
    // ofs << "ime,prezime,plata" << endl;
    // ofs.close();

    // ifstream ifs("fakulteti.csv");
    // string linija;

    // while(!ifs.eof()){
    //     ifs >> linija;
    //     Fakultet f(linija);
    //     // kao da smo napisali operator>>(ifs, linija)
    //     // getline(ifs, linija)
    //     cout << f.naziv << endl;
    //     cout << f.mesto << endl;
    // }

    // ifstream ifs("radnici.csv");
    // string linija;
    // double ukupnaPlata = 0;
    // while(!ifs.eof()){
    //     ifs >> linija;
    //     Radnik r(linija);
    //     cout << r.ime << endl;
    //     cout << r.prezime << endl;
    //     cout << r.plata << endl;
    //     ukupnaPlata += r.plata;
    //     cout << " provera toString" << endl;
    //     cout << r.to_string() << endl; 
    // }
    // cout << "Ukupna plata: " << ukupnaPlata << endl;

    // lepse napisano ovo pre
    vector<Radnik*> radnici;
    // citanje iz datoteke radnici
    // ---------------------------------------
    ifstream ifs("radnici.csv");
    string linija;
    while(!ifs.eof()){
        ifs >> linija;
        cout << linija << endl;
        Radnik *r = new Radnik(linija);
        radnici.push_back(r);
    }
    ifs.close();
    // racunanje ukupne plate
    // ---------------------------------------
    double ukupna_plata = 0;
    for(Radnik *el: radnici){
        ukupna_plata += el->plata;
    }
    cout << "Ukupna plata: " << ukupna_plata << endl;
    // povecaj svima platu za 10%
    for(Radnik *radnik: radnici){
        radnik->plata = radnik->plata * 1.1;
    }
    ofstream ofs("radnici2.csv");
    for(Radnik *r: radnici){
        ofs << r->to_string() << endl;
    }
    ofs.close();

    // string ime;
    // string prezime;
    // double plata;
    // double ukupnaPlata = 0;


    // int  i = 0;
    // while(!ifs.eof()){
    //     ifs >> ime >> prezime >> plata;

    //     cout << i++ << "----------------------" << endl;
    //     cout << ime << endl;
    //     cout << prezime << endl;
    //     cout << plata << endl;
    //     ukupnaPlata += plata;
    // }

    // pauza do 10:05


    // for(int i=0; i<100; i++){
    //     if(ifs.eof())
    //         break;

    //     ifs >> ime >> prezime >> plata;

    //     cout << i << "----------------------" << endl;
    //     cout << ime << endl;
    //     cout << prezime << endl;
    //     cout << plata << endl;
    //     ukupnaPlata += plata;

    // }

    // cout << "Ukupna plata: " << ukupnaPlata << endl;
    return 0;
}