#ifndef OSOBA_HPP
#define OSOBA_HPP

#include <string>
using namespace std;

class Osoba
{
private:
    string ime;
    string prezime;

public:
    Osoba();
    Osoba(const string &ime, const string &prezime);
    string getIme() const;
    void setIme(const string &ime);
    string getPrezime() const;
    void setPrezime(const string &prezime);
};

#endif