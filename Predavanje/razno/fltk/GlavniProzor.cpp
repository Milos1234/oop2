#include "GlavniProzor.hpp"

GlavniProzor::GlavniProzor(const string &naslov, Fl_Group *layout) : Fl_Window(400, 130, naslov.c_str()), layout(layout)
{
    if (layout != nullptr)
    {
        this->add(this->layout);
    }
}

void GlavniProzor::dodajKomponentu(Fl_Widget *komponenta)
{
    this->layout->add(komponenta);
}

void GlavniProzor::setLayout(Fl_Group *layout)
{
    if (this->layout != nullptr)
    {
        this->remove(this->layout);
    }
    this->layout = layout;
    this->add(this->layout);
}

int GlavniProzor::run()
{
    this->show();
    return Fl::run();
}