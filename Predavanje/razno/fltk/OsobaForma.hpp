#ifndef OSOBA_FORMA_HPP
#define OSOBA_FORMA_HPP

#include "Osoba.hpp"

#include <FL/Fl_Widget.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Button.H>

#include <vector>

using namespace std;

class OsobaForma : public Fl_Pack
{
protected:
    vector<Osoba*> &osobe;
    Fl_Input *imeInput;
    Fl_Input *prezimeInput;
    Fl_Button *dodajButton;

public:
    OsobaForma(vector<Osoba*> &osobe);
    static void onDodaj(Fl_Widget *, void *);
};

#endif