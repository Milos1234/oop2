/**
 * Napisati klasu Osoba sa atributima ime i prezime. Klasa ima konstruktore i get i set metode.
 * Napraviti prozor koji se sastoji iz forme za unos imena i prezimena.
 * Pri unosu podatak osoba se dodaje u vektor osoba.
 * Ispod forme se nalazi komponenta za prikaz svih uneti osoba, ova komponenta
 * se sastoji iz labela za ispis imena i prezime i dugmadi za prelazak na
 * sledeću i prethodnu osobu. Dugmad nemaju efekta u slučaju da nema prethodne,
 * odnosno sledeće osobe.
 */

#include "Osoba.hpp"

#include "GlavniProzor.hpp"
#include "OsobaForma.hpp"

#include <vector>

using namespace std;

int main()
{
    vector<Osoba*> osobe;

    OsobaForma *osobaForma = new OsobaForma(osobe);
    
    Fl_Pack *glavniProzorLayout = new  Fl_Pack(80, 10, 300, 800);
    glavniProzorLayout->spacing(30);

    GlavniProzor *glavniProzor = new GlavniProzor("Zadatak 2", glavniProzorLayout);
    
    glavniProzor->dodajKomponentu(osobaForma);    
    return glavniProzor->run();
}