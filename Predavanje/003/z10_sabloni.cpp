#include <iostream>
#include <string>
#include <vector>

using namespace std;

template <typename T> 
T maksimalan(T a, T b){
    if(a>b){
        return a;
    }
    else
    {
        return b;
    }    
}

template <class T>
class Stack{
private:
  vector<T> elementi;

public:
    void push(T el){
        elementi.push_back(el);
    }

    T pop(){
        T r = elementi.back();
        elementi.pop_back();
        return r;
    }

    void stanje(){
        cout << "-------STACK-----------"<<endl;
        for(T el : elementi){
            cout<< el << endl;
        }
    }
};


int main() {
    cout<<"12-10-2020 cetvrti cas " << endl;
    double r = maksimalan(2.5, 3.1);
    int t = maksimalan(2, 4);
    cout << r << " " << t  << endl;

    Stack<string> stack;
    stack.push("A");
    stack.push("B");
    stack.push("C");
    stack.stanje();
    cout << "pop: " << stack.pop() << endl;
    stack.stanje();

    Stack<int> intstack;
    intstack.push(2);
    intstack.push(3);
    intstack.push(9);
    intstack.stanje();
    cout << "pop: " << intstack.pop() << endl;
    intstack.stanje();



    return 0;
}