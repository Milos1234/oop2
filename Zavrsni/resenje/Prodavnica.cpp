#include "Prodavnica.hpp"

    Prodavnica::Prodavnica(){};
    Prodavnica::Prodavnica(string naziv, string adresa, int brojProdajnihMesta):naziv(naziv), adresa(adresa), brojProdajnihMesta(brojProdajnihMesta){};
    string Prodavnica::getNaziv(){return naziv;};
    string Prodavnica::getAdresa(){return adresa;};
    int Prodavnica::getBrojProdajnihMesta(){return brojProdajnihMesta;};
    
    string Prodavnica::getTip(){return "prodavnica";};
    
    void Prodavnica::setNaziv(string naziv){this->naziv=naziv;};
    void Prodavnica::setWebAdresa(string adresa){this->adresa = adresa;};
    void Prodavnica::setBrojProdajnihMesta(int brojProdajnihMesta){this->brojProdajnihMesta = brojProdajnihMesta;};
    

    
    void Prodavnica::zaposli(Radnik *radnik){
       
    //    if (radnik->getTip() == "prodavac"){
    //        if(radnici.size() < brojProdajnihMesta) {
    //     radnici.push_back(radnik); }}
    //     else {
    //         cout<<"Postoji previse zaposlenih!"<<endl;
    //     }

       radnici.push_back(radnik);

        
    };
    
    
    void Prodavnica::dajOtkaz(int indeks){
            bool nePostoji = false;
    for (int i = 0; i < radnici.size(); i++)
    {
        if (i == indeks - 1)
        {
            radnici.erase(radnici.begin() + indeks - 1);
            
            nePostoji = true;
        }
    }

    if (!nePostoji)
    {
        cout << "Nije pronadjen radnik sa zadatim indeksom!" << endl;
    }

    };
    double Prodavnica::ukupniTroskovi(){
        double ukupniTroskovi;
            for (Radnik *r : radnici){
            ukupniTroskovi+= r->ukupnaPlata(); 
            
    } return ukupniTroskovi;
};
    
    void Prodavnica::upisUDatoteku(ostream &output){
    output<<getTip()<<"|"<<naziv<<"|"<<adresa<<endl;
       for (Radnik *r : radnici){          
        r->upisUDatoteku(output);
    }};



    

  