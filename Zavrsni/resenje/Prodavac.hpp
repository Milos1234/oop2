#ifndef PRODAVAC_HPP
#define PRODAVAC_HPP
#include <iostream>
#include <string>
#include "Radnik.hpp"

using namespace std;


class Prodavac: public Radnik
{
private:
    
    string sifraKase;
    double radnoVreme;
    

public:
    Prodavac();
    Prodavac(string ime, string prezime, double visinaPlate, double porezNaDoprinose,string sifraKase, double radnoVreme);
 
    string getSifraKase();
    void setSifraKase(string sifraKase);
    double getRadnoVreme();
    void setRadnoVreme(double radnoVreme);
 
    virtual string getTip();
    virtual double ukupnaPlata();
    virtual void upisUDatoteku(ostream &output); 

    virtual ~Prodavac();
};
#endif