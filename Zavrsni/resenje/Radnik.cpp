    #include "Radnik.hpp"
    Radnik::Radnik(){};
    Radnik::Radnik(string ime, string prezime, double visinaPlate, double porezNaDoprinose):ime(ime),prezime(prezime),visinaPlate(visinaPlate),porezNaDoprinose(porezNaDoprinose){};
 
    string Radnik::getIme(){  return ime;};
    void Radnik::setIme(string ime){this->ime = ime;};
    string Radnik::getPrezime(){return prezime;};
    void Radnik::setIPrezime(string prezime){this->prezime = prezime;};
    double Radnik::getVisinaPlate(){return visinaPlate;};
    void Radnik::setVisinaPlate(double visinaPlate){this->visinaPlate = visinaPlate;};
    double Radnik::getPorezNaDoprinose(){return porezNaDoprinose;};
    void Radnik::setPorezNaDoprinose(double porezNaDoprinose){this->porezNaDoprinose=porezNaDoprinose;};
    string Radnik::getTip(){return "radnik";}; 
    double Radnik::ukupnaPlata(){
        double ukupnaPlata;
        ukupnaPlata = visinaPlate + visinaPlate * porezNaDoprinose;
        return ukupnaPlata;
    };
    void Radnik::upisUDatoteku(ostream &output){ // importovanje metode 
         output<<getTip()<<"|"<<ime<<"|"<<prezime<<"|"<<visinaPlate<<"|"<<porezNaDoprinose<<"|"; }
    
    
    Radnik::~Radnik(){}