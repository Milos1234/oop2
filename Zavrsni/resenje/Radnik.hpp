#ifndef RADNIK_HPP
#define RADNIK_HPP
#include <iostream>
#include <string>
using namespace std;
class Radnik
{
private:
    
    string ime;
    string prezime;
    double visinaPlate;
    double porezNaDoprinose;

public:
    Radnik();
    Radnik(string ime, string prezime, double visinaPlate, double porezNaDoprinose);
 
    string getIme();
    void setIme(string ime);
    string getPrezime();
    void setIPrezime(string prezime);
    double getVisinaPlate();
    void setVisinaPlate(double visinaPlate);
    double getPorezNaDoprinose();
    void setPorezNaDoprinose(double porezNaDoprinose);
    virtual string getTip();
    virtual double ukupnaPlata()=0; 
    virtual void upisUDatoteku(ostream &output); 
    
    virtual ~Radnik();
    
};
#endif
