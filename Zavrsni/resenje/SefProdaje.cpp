#include "SefProdaje.hpp"

    SefProdaje::SefProdaje():Radnik(){};
    SefProdaje::SefProdaje(string ime, string prezime, double visinaPlate, double porezNaDoprinose, double bonus):Radnik(ime,prezime,visinaPlate,porezNaDoprinose),bonus(bonus){};
 
 
    double SefProdaje::getBonus(){return bonus;};
    void SefProdaje::setBonus(double bonus){this->bonus = bonus;};
 
    string SefProdaje::getTip(){ return "sef";};
    
    double SefProdaje::SefProdaje::ukupnaPlata(){
        double ukupnaPlataSefa = Radnik::ukupnaPlata();
        ukupnaPlataSefa += bonus;
        return ukupnaPlataSefa;

    };
    void SefProdaje::upisUDatoteku(ostream &output){
        Radnik::upisUDatoteku(output);
        output<<bonus<<"|"<<endl;
    };
    SefProdaje::~SefProdaje(){};