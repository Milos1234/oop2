#include <iostream>
#include <string>
#include "Prodavnica.hpp"
#include "Prodavac.hpp"
#include "Radnik.hpp"
#include "SefProdaje.hpp"
#include <fstream>

using namespace std;

ostream &operator<<(ostream &output, Prodavnica *prodavnica)
{
    prodavnica->upisUDatoteku(output);

    return output;
}

vector<string> tokenizacija(string &linija, string separator)
{
    vector<string> delovi;
    int pozicija;
    while (true)
    {
        pozicija = linija.find(separator);
        if (pozicija == -1)
        {
            break;
        }
        string deo = linija.substr(0, pozicija);
        delovi.push_back(deo);
        linija = linija.substr(pozicija + 1, linija.length());
    }
    delovi.push_back(linija);
    return delovi;
}

istream &operator>>(istream &input, Prodavnica* &prodavnica)
{

    string tip;
    while (!input.eof())
    {
        string linija;
        Radnik *noviRadnik;

        getline(input, tip, '|');

        getline(input, linija, '\n');

        if (tip == "prodavnica")
        {

            vector<string> delovi = tokenizacija(linija, "|");
       
            prodavnica = new Prodavnica(delovi[0], delovi[1], stoi(delovi[2]));
            
        }

        else if (tip == "prodavac")
        {

            vector<string> delovi = tokenizacija(linija, ";");
            
            noviRadnik = new Prodavac(delovi[0], delovi[1], stod(delovi[2]), stod(delovi[3]),delovi[4], stod(delovi[5]));
           
        }
        else if (tip == "sef")
        {

            vector<string> delovi = tokenizacija(linija, ";");
            
            noviRadnik = new SefProdaje(delovi[0], delovi[1], stod(delovi[2]), stod(delovi[3]), stod(delovi[5]));
            
        }

        prodavnica->zaposli(noviRadnik);
        
    }
    return input;
}
int main()
{
    

    Prodavnica *p = new Prodavnica("Univer", "Bulevar oslobodjenja",20);
    SefProdaje *sef = new SefProdaje("Milorad", "Stefanovic", 500.0, 10.0, 2.0);
    Prodavac *prodavac = new Prodavac("Petar", "Petrovic", 600.0, 10.0,"kod", 8.0);
    p->zaposli(sef);
    p->zaposli(prodavac);


    ofstream out;
    out.open("prodavnica.csv"); 
    out<<p;
    out.close();

    fstream file;
    file.open("prodavnica.csv");
    string linija;
    while(!file.eof()){
        file >> linija;
        cout << linija << endl;
    }
    file.close();

    // Prodavnica *prodavnica;
    // fstream file("prodavnica.csv");
    // file >> prodavnica;

    // file.close();

    return 0;
}