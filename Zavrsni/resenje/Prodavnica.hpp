#ifndef PRODAVNICA_HPP
#define PRODAVNICA_HPP
#include <iostream>
#include <string>
#include "Radnik.hpp"
#include "Prodavac.hpp"
#include "SefProdaje.hpp"
#include <vector>

using namespace std;

class Prodavnica{
    protected:
        string naziv;
        string adresa;
        int brojProdajnihMesta;
        vector<Radnik*> radnici;
    public:
        Prodavnica();
        Prodavnica(string naziv, string adresa, int brojProdajnihMesta);
        string getNaziv();
        string getAdresa();
        int getBrojProdajnihMesta();
        
        string getTip();
        
        void setNaziv(string naziv);
        void setWebAdresa(string adresa);
        void setBrojProdajnihMesta(int brojProdajnihMesta);
      

        
        void zaposli(Radnik *radnik);
        void dajOtkaz(int indeks);
        double ukupniTroskovi();
        virtual void upisUDatoteku(ostream &output);
        friend ostream &operator<<(ostream &output, Prodavnica *prodavnica);
        friend istream &operator>>(istream &input, Prodavnica* &prodavnica);
   
        ~Prodavnica();


        

};

#endif