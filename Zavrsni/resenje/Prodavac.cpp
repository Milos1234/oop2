#include "Prodavac.hpp"

    Prodavac::Prodavac():Radnik(){};
    Prodavac::Prodavac(string ime, string prezime, double visinaPlate, double porezNaDoprinose,string sifraKase, double radnoVreme):
                                        Radnik(ime,prezime,visinaPlate,porezNaDoprinose), sifraKase(sifraKase), radnoVreme(radnoVreme){};
 
    string Prodavac::getSifraKase(){ return sifraKase;};
    void Prodavac::setSifraKase(string sifraKase){this->sifraKase = sifraKase;};
    double Prodavac::getRadnoVreme(){return radnoVreme;};
    void Prodavac::setRadnoVreme(double radnoVreme){this->radnoVreme = radnoVreme;};
 
    string Prodavac::getTip(){return "prodavac";};
    double Prodavac::ukupnaPlata(){
        double ukupnaPlataProdavca = Radnik::ukupnaPlata();
        
        if (radnoVreme > 8){

            double bonus = (2 * ukupnaPlataProdavca)/100;
            double prekovremeniSati = radnoVreme - 8;
            ukupnaPlataProdavca += bonus * prekovremeniSati;
        
        return ukupnaPlataProdavca;}
        else {
            cout<<"Nemate prekovremenih sati!";
            return ukupnaPlataProdavca;
        }
        };
    void Prodavac::upisUDatoteku(ostream &output){
        Radnik::upisUDatoteku(output);
        output<<sifraKase<<"|"<<radnoVreme<<endl;
    };

    Prodavac:: ~Prodavac(){};