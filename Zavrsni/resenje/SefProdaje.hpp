#ifndef SEF_PRODAJE_HPP
#define SEF_PRODAJE_HPP
#include <iostream>
#include <string>
#include "Radnik.hpp"

using namespace std;


class SefProdaje: public Radnik
{
private:
    
    
    double bonus;
    

public:
    SefProdaje();
    SefProdaje(string ime, string prezime, double visinaPlate, double porezNaDoprinose, double bonus);
 
 
    double getBonus();
    void setBonus(double bonus);
 
    virtual string getTip();
    virtual double ukupnaPlata();
    virtual void upisUDatoteku(ostream &output);

    virtual ~SefProdaje();
};
#endif