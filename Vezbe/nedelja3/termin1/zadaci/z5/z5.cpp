/**
 * Prepraviti prethodni zadatak dodavanjem
 * widgeta za prikaz troškova i budžeta firme.
 * Ovaj widget ispisuje troškove i budžet
 * razdovjene simbolom /, npr.: 20000/12000.
 * gde prvi broj predstavlja troškove a drugi budžet.
 * Ispod ovog ispisa nacrtati pravougaonik čija dužina
 * je prorporcionalna visini budžeta i čija boja je zelena.
 * Preko ovog pravougaonika iscrtati crveni pravougaonik
 * čija je dužina proporcionalna troškovima. Na krajevima
 * ovih prvaougaonika ispisati troškove odnosno budžet.
 */