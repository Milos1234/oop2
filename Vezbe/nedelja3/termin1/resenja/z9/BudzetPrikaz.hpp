#ifndef BUDZET_PRIKAZ_HPP
#define BUDZET_PRIKAZ_HPP

#include <sstream>

#include <FL/Fl_Group.H>
#include <FL/Fl_Box.H>

using namespace std;

class BudzetPrikaz : Fl_Group
{
private:
    double troskovi;
    double budzet;
    Fl_Box *budzetBox;
    Fl_Box *troskoviBox;

protected:
    void azurirajPrikaz();

public:
    BudzetPrikaz(int x, int y, int w, int h, double budzet, double troskovi);
    double getBudzet();
    void setBudzet(double budzet);
    double getTroskovi();
    void setTroskovi(double troskovi);
    virtual ~BudzetPrikaz();
};

#endif