#include "TabelaOsoba.hpp"

TabelaOsoba::TabelaOsoba(int x, int y, int w, int h,
                         ApstraktniTabelarniModel<Osoba *> *model) : Fl_Table_Row(x, y, w, h), model(model)
{
    this->end();
    col_resize_min(10);
    col_resize(1);
    col_header(1);
    row_header(1);
    model->pretplatiSlusaoca(this);
}
void TabelaOsoba::draw_cell(TableContext context, int red, int kolona, int x, int y, int w, int h)
{
    switch (context)
    {
    case CONTEXT_COL_HEADER:
    {
        fl_push_clip(x, y, w, h);
        {
            fl_draw_box(FL_THIN_UP_BOX, x, y, w, h, col_header_color());
            fl_color(FL_BLACK);
            fl_draw(model->horizontalnoZaglavlje(kolona).c_str(), x, y, w, h, FL_ALIGN_CENTER);
        }
        fl_pop_clip();
        return;
    }
    case CONTEXT_ROW_HEADER:
    {
        fl_push_clip(x, y, w, h);
        {
            fl_draw_box(FL_THIN_UP_BOX, x, y, w, h, col_header_color());
            fl_color(FL_BLACK);
            fl_draw(model->vertikalnoZaglavlje(red).c_str(), x - 2, y, w, h, FL_ALIGN_RIGHT);
        }
        fl_pop_clip();
        return;
    }
    case CONTEXT_CELL:
    {
        fl_push_clip(x, y, w, h);
        {
            fl_color(row_selected(red) ? FL_SELECTION_COLOR : FL_WHITE);
            fl_rectf(x, y, w, h);
            fl_color(row_selected(red) ? FL_WHITE : FL_BLACK);
            fl_draw(model->dobaviTekst(red, kolona).c_str(), x + 2, y, w, h, FL_ALIGN_LEFT);
            fl_color(color());
            fl_rect(x, y, w, h);
        }
        fl_pop_clip();
        return;
    }
    case CONTEXT_STARTPAGE:
    case CONTEXT_TABLE:
    case CONTEXT_ENDPAGE:
    case CONTEXT_RC_RESIZE:
    case CONTEXT_NONE:
        return;
    }
}
void TabelaOsoba::elementUmetnut(int red, Osoba *element)
{
    rows(model->brojRedova());
    cols(model->brojKolona());
}
void TabelaOsoba::elementUklonjen(int red)
{
    rows(model->brojRedova());
    cols(model->brojKolona());
}
TabelaOsoba::~TabelaOsoba()
{
}