#ifndef APSTRAKTNI_MODEL_HPP
#define APSTRAKTNI_MODEL_HPP

#include <vector>
#include <algorithm>

#include "ApstraktniPrikaz.hpp"

using namespace std;

template <typename T>
class ApstraktniModel
{
private:
    vector<ApstraktniPrikaz<T> *> slusaoci;

public:
    virtual int brojElemenata() = 0;
    virtual T dobaviElement(int indeks) = 0;
    virtual void umetniElement(int indeks, T element) = 0;
    virtual void ukloniElement(int indeks) = 0;
    void pretplatiSlusaoca(ApstraktniPrikaz<T> *slusalac);
    void odjaviSlusaoca(ApstraktniPrikaz<T> *slusalac);
    virtual ~ApstraktniModel();
};

template <typename T>
void ApstraktniModel<T>::umetniElement(int indeks, T element)
{
    for (ApstraktniPrikaz<T> *ap : slusaoci)
    {
        ap->elementUmetnut(indeks, element);
    }
}

template <typename T>
void ApstraktniModel<T>::ukloniElement(int indeks)
{
    for (ApstraktniPrikaz<T> *ap : slusaoci)
    {
        ap->elementUklonjen(indeks);
    }
}

template <typename T>
void ApstraktniModel<T>::pretplatiSlusaoca(ApstraktniPrikaz<T> *slusalac)
{
    slusaoci.push_back(slusalac);
}
template <typename T>
void ApstraktniModel<T>::odjaviSlusaoca(ApstraktniPrikaz<T> *slusalac)
{
    vector<ApstraktniPrikaz<T> *> iterator = find(slusaoci->begin, slusaoci->end());
    if (iterator != slusaoci->end())
    {
        slusaoci.erase(iterator);
    }
}
template <typename T>
ApstraktniModel<T>::~ApstraktniModel()
{
}

#endif