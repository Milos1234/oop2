/**
 * Proširiti prethodni zadatak dodavanjem
 * dugmeta za brisanje elementa iz tabele i 
 * dijaloga za ispis detalja osobe izabrane
 * iz tabele.
 */

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/fl_ask.H>

#include "FormaOsoba.hpp"
#include "PrikazOsoba.hpp"
#include "BudzetPrikaz.hpp"
#include "TabelaOsoba.hpp"

#include "KolekcijaOsoba.hpp"

using namespace std;

ostream &operator<<(ostream &output, Osoba *osoba)
{
    osoba->zapisi(output);
    return output;
}

istream &operator>>(istream &input, Osoba *osoba)
{
    osoba->procitaj(input);
    return input;
}

ostream &operator<<(ostream &output, KolekcijaOsoba &osobe)
{
    osobe.zapisi(output);
    return output;
}

istream &operator>>(istream &input, KolekcijaOsoba &osobe)
{
    osobe.procitaj(input);
    return input;
}

ostream &operator<<(ostream &output, KolekcijaOsoba *osobe)
{
    osobe->zapisi(output);
    return output;
}

istream &operator>>(istream &input, KolekcijaOsoba *osobe)
{
    osobe->procitaj(input);
    return input;
}

void cuvanjeIzmena(Fl_Widget *widget, void *data)
{
    int odgovor = fl_choice("Да ли желите да сачувате измене и напустите програм?", "Da", "Ne", "Odustani");
    if (odgovor == 0)
    {
        KolekcijaOsoba *osobe = (KolekcijaOsoba *)data;
        ofstream datoteka("osobe.txt");
        datoteka << osobe;
        datoteka.close();
    }
    if (odgovor < 2)
    {
        widget->hide();
    }
}

struct Event
{
    KolekcijaOsoba *osobe;
    TabelaOsoba *tabela;
};

void ukloni(Fl_Widget *widget, void *data)
{
    Event *event = (Event *)data;
    int startRow;
    int endRow;
    int colLeft;
    int colRight;
    event->tabela->get_selection(startRow, colLeft, endRow, colRight);
    for (int i = endRow; i >= startRow; i--)
    {
        event->osobe->ukloniRed(i);
    }
}

int main()
{
    KolekcijaOsoba osobe;

    Fl_Window *window = new Fl_Window(800, 600, "Zadatak 5");
    FormaOsoba *formaOsoba = new FormaOsoba(0, 50, 300, 240, "Podaci o osobi", &osobe);
    PrikazOsoba *prikazOsoba = new PrikazOsoba(400, 50, 300, 150, &osobe);
    BudzetPrikaz *budzetPrikaz = new BudzetPrikaz(400, 180, 300, 30, 120000, 40000);
    TabelaOsoba *tabelaOsoba = new TabelaOsoba(10, 310, 780, 280, &osobe);
    Fl_Button *button = new Fl_Button(400, 260, 300, 30, "Ukloni");
    Event *ev = new Event();
    ev->osobe = &osobe;
    ev->tabela = tabelaOsoba;
    button->callback(ukloni, ev);
    window->end();

    ifstream datoteka("osobe.txt");
    datoteka >> osobe;
    datoteka.close();
    window->callback(cuvanjeIzmena, &osobe);

    window->show();
    return Fl::run();
}