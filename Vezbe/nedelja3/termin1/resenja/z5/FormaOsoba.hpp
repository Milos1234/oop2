#ifndef FORMA_OSOBA_HPP
#define FORMA_OSOBA_HPP

#include <FL/Fl_Input.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Group.H>

#include <vector>
#include <string>
#include <fstream>
#include <sstream>

#include "Osoba.hpp"
#include "Radnik.hpp"
#include "Vozac.hpp"
#include "Komercijalista.hpp"

using namespace std;

ostream &operator<<(ostream &output, vector<Osoba*> &osobe);

class FormaOsoba : public Fl_Group
{
protected:
    Fl_Input *imeInput;
    Fl_Input *prezimeInput;
    Fl_Choice *tipInput;
    Fl_Float_Input *visinaPlateInput;
    Fl_Choice *kategorijaInput;
    Fl_Input *odeljenjeInput;
    Fl_Button *dodaj;
    vector<Osoba*> *osobe;

    static void promeniTip(Fl_Widget *widget, void *data);

public:
    FormaOsoba(int x, int y, int width, int height, const char *label, vector<Osoba*> *osobe);
    virtual ~FormaOsoba();

    static void dodajOsobu(Fl_Widget *widget, void *data);
};

#endif