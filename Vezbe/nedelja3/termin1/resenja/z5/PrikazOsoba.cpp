#include "PrikazOsoba.hpp"

PrikazOsoba::PrikazOsoba(int x, int y,
                         int w, int h,
                         vector<Osoba*> *osobe) : Fl_Group(x, y, w, h),
                                                 osobe(osobe), trenutni(-1)
{
    imeOutput = new Fl_Output(x + 50, y + 0, 200, 30);
    prezimeOutput = new Fl_Output(x + 50, y + 40, 200, 30);

    prethodniButton = new Fl_Button(x, y, 45, 70, "@<-");
    sledeciButton = new Fl_Button(x + 255, y, 45, 70, "@->");

    prethodniButton->callback(predjiNaPrethodni, this);
    sledeciButton->callback(predjiNaSledeci, this);

    if (osobe->size() > 0)
    {
        trenutni = 0;
    }

    postaviPrikaz(trenutni);
    proveraDugmica();

    this->end();
}

void PrikazOsoba::azurirajLabelu() {
    stringstream sstream;
    sstream << "Osoba " << trenutni+1 << "/" << osobe->size();
    this->copy_label(sstream.str().c_str());
}

void PrikazOsoba::proveraDugmica()
{
    if (trenutni <= 0)
    {
        prethodniButton->deactivate();
    }
    else
    {
        prethodniButton->activate();
    }
    if (trenutni >= osobe->size() - 1)
    {
        sledeciButton->deactivate();
    }
    else
    {
        sledeciButton->activate();
    }
}

void PrikazOsoba::postaviPrikaz(int indeks)
{
    if (indeks >= 0 && indeks < osobe->size())
    {
        trenutni = indeks;
        imeOutput->value(osobe->at(indeks)->getIme().c_str());
        prezimeOutput->value(osobe->at(indeks)->getPrezime().c_str());
    }
    azurirajLabelu();
}

void PrikazOsoba::predjiNaSledeci(Fl_Widget *widget, void *data)
{
    PrikazOsoba *prikazOsoba = (PrikazOsoba *)data;
    if (prikazOsoba->trenutni + 1 < prikazOsoba->osobe->size())
    {
        prikazOsoba->trenutni++;
        prikazOsoba->postaviPrikaz(prikazOsoba->trenutni);
    }

    prikazOsoba->proveraDugmica();
}

void PrikazOsoba::predjiNaPrethodni(Fl_Widget *widget, void *data)
{
    PrikazOsoba *prikazOsoba = (PrikazOsoba *)data;
    if (prikazOsoba->trenutni - 1 >= 0)
    {
        prikazOsoba->trenutni--;
        prikazOsoba->postaviPrikaz(prikazOsoba->trenutni);
    }

    prikazOsoba->proveraDugmica();
}

PrikazOsoba::~PrikazOsoba()
{
}