#include "FormaOsoba.hpp"

FormaOsoba::FormaOsoba(int x, int y,
                       int width, int height,
                       const char *label,
                       vector<Osoba *> *osobe) : Fl_Group(x, y, width, height, label),
                                                 osobe(osobe)
{
    imeInput = new Fl_Input(x + 100, y + 10, 200, 30, "Ime:");
    prezimeInput = new Fl_Input(x + 100, y + 50, 200, 30, "Prezime:");
    tipInput = new Fl_Choice(x + 100, y + 90, 200, 30, "Tip:");
    visinaPlateInput = new Fl_Float_Input(x + 100, y + 130, 200, 30, "Visina plate:");
    kategorijaInput = new Fl_Choice(x + 100, y + 170, 200, 30, "Kategorija:");
    odeljenjeInput = new Fl_Input(x + 100, y + 170, 200, 30, "Odeljenje:");
    dodaj = new Fl_Button(x + 100, y + 210, 100, 30, "Dodaj");

    tipInput->add("Osoba|Radnik|Vozac|Komercijalista");
    tipInput->value(0);
    kategorijaInput->add("A|B|C");

    tipInput->callback(promeniTip, this);
    dodaj->callback(FormaOsoba::dodajOsobu, this);

    tipInput->do_callback();
    this->end();
}

void FormaOsoba::dodajOsobu(Fl_Widget *widget, void *data)
{
    FormaOsoba *forma = (FormaOsoba *)data;
    Osoba *novaOsoba;
    if (forma->tipInput->value() == 0)
    {
        novaOsoba = new Osoba(forma->imeInput->value(),
                              forma->prezimeInput->value());
    }
    else if (forma->tipInput->value() >= 0)
    {
        stringstream sstream(forma->visinaPlateInput->value());
        double visinaPlate;
        sstream >> visinaPlate;
        if (forma->tipInput->value() == 1)
        {
            novaOsoba = new Radnik(forma->imeInput->value(),
                                   forma->prezimeInput->value(),
                                   visinaPlate);
        }
        else if (forma->tipInput->value() == 2)
        {
            novaOsoba = new Vozac(forma->imeInput->value(),
                                  forma->prezimeInput->value(),
                                  visinaPlate,
                                  forma->kategorijaInput->text(forma->kategorijaInput->value()));
        }
        else if (forma->tipInput->value() == 3)
        {
            novaOsoba = new Komercijalista(forma->imeInput->value(),
                                  forma->prezimeInput->value(),
                                  visinaPlate,
                                  forma->odeljenjeInput->value());
        }
    }

    forma->osobe->push_back(novaOsoba);
    ofstream out("osobe.txt");
    out << *(forma->osobe);
    out.close();
}

void FormaOsoba::promeniTip(Fl_Widget *widget, void *data)
{
    FormaOsoba *forma = (FormaOsoba *)data;
    if (forma->tipInput->value() > 0)
    {
        forma->visinaPlateInput->show();
        forma->visinaPlateInput->value("");
    }
    else
    {
        forma->visinaPlateInput->hide();
    }

    if (forma->tipInput->value() == 2)
    {
        forma->kategorijaInput->value(0);
        forma->kategorijaInput->show();
    }
    else
    {
        forma->kategorijaInput->hide();
    }

    if (forma->tipInput->value() == 3)
    {
        forma->odeljenjeInput->value("");
        forma->odeljenjeInput->show();
    }
    else
    {
        forma->odeljenjeInput->hide();
    }
}

FormaOsoba::~FormaOsoba()
{
}