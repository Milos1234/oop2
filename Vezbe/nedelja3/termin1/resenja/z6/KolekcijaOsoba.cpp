#include "KolekcijaOsoba.hpp"

KolekcijaOsoba::KolekcijaOsoba() : ApstraktniModel<Osoba *>()
{
}
int KolekcijaOsoba::brojElemenata()
{
    return osobe.size();
}
Osoba *KolekcijaOsoba::dobaviElement(int indeks)
{
    return osobe.at(indeks);
}
void KolekcijaOsoba::umetniElement(int indeks, Osoba *element)
{
    osobe.insert(osobe.begin() + indeks, element);
    ApstraktniModel::umetniElement(indeks, element);
}

void KolekcijaOsoba::ukloniElement(int indeks)
{
    osobe.erase(osobe.begin() + indeks);
    ApstraktniModel::ukloniElement(indeks);
}

void KolekcijaOsoba::zapisi(ostream &output)
{
    output << osobe.size() << endl;
    for (int i = 0; i < osobe.size(); i++)
    {
        output << osobe.at(i) << endl;
    }
}
void KolekcijaOsoba::procitaj(istream &input)
{
    int brojOsoba;
    input >> brojOsoba;
    string tip;
    for (int i = 0; i < brojOsoba; i++)
    {
        Osoba *novaOsoba;
        input >> tip;
        if (tip == "osoba")
        {
            novaOsoba = new Osoba();
        }
        else if (tip == "radnik")
        {
            novaOsoba = new Radnik();
        }
        else if (tip == "vozac")
        {
            novaOsoba = new Vozac();
        }
        else if (tip == "komercijalista")
        {
            novaOsoba = new Komercijalista();
        }
        input >> novaOsoba;
        umetniElement(osobe.size(), novaOsoba);
    }
}

KolekcijaOsoba::~KolekcijaOsoba()
{
}