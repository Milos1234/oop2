#ifndef PRIKAZ_OSOBA_HPP
#define PRIKAZ_OSOBA_HPP

#include <sstream>

#include <FL/Fl_Group.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Button.H>

#include "ApstraktniModel.hpp"
#include "ApstraktniPrikaz.hpp"

#include "Osoba.hpp"

using namespace std;

class PrikazOsoba : public Fl_Group, protected ApstraktniPrikaz<Osoba *>
{
protected:
    Fl_Output *imeOutput;
    Fl_Output *prezimeOutput;
    Fl_Button *sledeciButton;
    Fl_Button *prethodniButton;

    int trenutni = -1;
    ApstraktniModel<Osoba *> *model;

    void azurirajLabelu();
    void proveraDugmica();
    void postaviPrikaz(int indeks);
    virtual void elementUmetnut(int indeks, Osoba* element);
    virtual void elementUklonjen(int indeks);

public:
    PrikazOsoba(int x, int y, int w, int h, ApstraktniModel<Osoba *> *model);
    virtual ~PrikazOsoba();

    static void predjiNaSledeci(Fl_Widget *widget, void *data);
    static void predjiNaPrethodni(Fl_Widget *widget, void *data);
};

#endif