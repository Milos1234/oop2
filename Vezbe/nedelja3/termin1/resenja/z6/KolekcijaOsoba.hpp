#ifndef KOLEKCIJA_OSOBA_HPP
#define KOLEKCIJA_OSOBA_HPP

#include <vector>
#include "ApstraktniModel.hpp"
#include "Osoba.hpp"
#include "Radnik.hpp"
#include "Vozac.hpp"
#include "Komercijalista.hpp"

using namespace std;

class KolekcijaOsoba : public ApstraktniModel<Osoba *>
{
private:
    vector<Osoba *> osobe;

public:
    KolekcijaOsoba();
    virtual int brojElemenata();
    virtual Osoba *dobaviElement(int indeks);
    virtual void umetniElement(int indeks, Osoba *element);
    virtual void ukloniElement(int indeks);
    void zapisi(ostream &output);
    void procitaj(istream &input);
    virtual ~KolekcijaOsoba();

    friend ostream &operator<<(ostream &output, KolekcijaOsoba &kolekcijaOsoba);
    friend istream &operator>>(istream &input, KolekcijaOsoba &kolekcijaOsoba);
    friend ostream &operator<<(ostream &output, KolekcijaOsoba *kolekcijaOsoba);
    friend istream &operator>>(istream &input, KolekcijaOsoba *kolekcijaOsoba);
};

#endif