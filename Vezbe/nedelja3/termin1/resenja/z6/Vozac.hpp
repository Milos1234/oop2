#ifndef VOZAC_HPP
#define VOZAC_HPP

#include "Radnik.hpp"

class Vozac : public Radnik
{
private:
    string kategorija;

public:
    Vozac();
    Vozac(string ime, string prezime, double visinaPlate, string kategorija);
    string getKategorija();
    void setKategorija(string kategorija);
    virtual string getTip();
    virtual void zapisi(ostream &output);
    virtual void procitaj(istream &input);
    virtual ~Vozac();
};

#endif