/**
 * Napisati klasu Osoba sa atributima ime i prezime. Klasa ima konstruktore i get i set metode.
 * Napraviti prozor koji se sastoji iz forme za unos imena i prezimena.
 * Pri unosu podatak osoba se dodaje u vektor osoba.
 * Ispod forme se nalazi komponenta za prikaz svih uneti osoba, ova komponenta
 * se sastoji iz labela za ispis imena i prezime i dugmadi za prelazak na
 * sledeću i prethodnu osobu. Dugmad nemaju efekta u slučaju da nema prethodne,
 * odnosno sledeće osobe.
 */

#include <FL/Fl.H>
#include <FL/Fl_Window.H>

#include "FormaOsoba.hpp"
#include "PrikazOsoba.hpp"
#include "Osoba.hpp"

using namespace std;

int main()
{
    vector<Osoba> osobe;

    osobe.push_back(Osoba("Ime1", "Prezime1"));
    osobe.push_back(Osoba("Ime2", "Prezime2"));

    Fl_Window *window = new Fl_Window(800, 600, "Zadatak 2");
    FormaOsoba *formaOsoba = new FormaOsoba(0, 50, 300, 120, "Podaci o osobi", &osobe);
    PrikazOsoba *prikazOsoba = new PrikazOsoba(400, 50, 300, 70, &osobe);

    window->end();
    window->show();
    return Fl::run();
}