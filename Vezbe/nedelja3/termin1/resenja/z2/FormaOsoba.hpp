#ifndef FORMA_OSOBA_HPP
#define FORMA_OSOBA_HPP

#include <FL/Fl_Input.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Group.H>

#include <vector>
#include <string>

#include "Osoba.hpp"

using namespace std;

class FormaOsoba : public Fl_Group
{
protected:
    Fl_Input *imeInput;
    Fl_Input *prezimeInput;
    Fl_Button *dodaj;
    vector<Osoba> *osobe;

public:
    FormaOsoba(int x, int y, int width, int height, const char *label, vector<Osoba> *osobe);
    virtual ~FormaOsoba();

    static void dodajOsobu(Fl_Widget *widget, void *data);
};

#endif