#include "Vozac.hpp"

Vozac::Vozac() : Radnik()
{
}
Vozac::Vozac(string ime, string prezime,
             double visinaPlate, string kategorija) : Radnik(ime, prezime, visinaPlate),
                                                      kategorija(kategorija)
{
}
string Vozac::getKategorija()
{
    return kategorija;
}
void Vozac::setKategorija(string kategorija)
{
    this->kategorija = kategorija;
}
string Vozac::getTip()
{
    return "vozac";
}
void Vozac::zapisi(ostream &output)
{
    Radnik::zapisi(output);
    output << " " << kategorija;
}
void Vozac::procitaj(istream &input)
{
    Radnik::procitaj(input);
    input >> kategorija;
}

Vozac::~Vozac()
{
}