#ifndef RADNIK_HPP
#define RADNIK_HPP

#include "Osoba.hpp"

class Radnik : public Osoba
{
private:
    double visinaPlate;

public:
    Radnik();
    Radnik(string ime, string prezime, double visinaPlate);
    double getVisinaPlate();
    void setVisinaPLate(double visinaPlate);
    virtual string getTip();
    virtual void zapisi(ostream &output);
    virtual void procitaj(istream &input);
    virtual ~Radnik();
};

#endif