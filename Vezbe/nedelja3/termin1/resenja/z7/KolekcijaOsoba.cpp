#include "KolekcijaOsoba.hpp"

KolekcijaOsoba::KolekcijaOsoba() : ApstraktniTabelarniModel<Osoba *>()
{
}
int KolekcijaOsoba::brojRedova()
{
    return osobe.size();
}

int KolekcijaOsoba::brojKolona()
{
    return 3;
}
Osoba *KolekcijaOsoba::dobaviRed(int indeks)
{
    return osobe.at(indeks);
}
string KolekcijaOsoba::dobaviTekst(int red, int kolona)
{
    Osoba *o = osobe.at(red);
    if (kolona == 0)
    {
        return o->getIme();
    }
    else if (kolona == 1)
    {
        return o->getPrezime();
    }
    else if (kolona == 2)
    {
        return o->getTip();
    }
    return "";
}
string KolekcijaOsoba::horizontalnoZaglavlje(int kolona)
{
    if (kolona == 0)
    {
        return "Ime";
    }
    else if (kolona == 1)
    {
        return "Prezime";
    }
    else if (kolona == 2)
    {
        return "Tip";
    }
    return "";
}
string KolekcijaOsoba::vertikalnoZaglavlje(int red)
{  
    stringstream sstream;
    sstream << red+1;
    return sstream.str();
}
void KolekcijaOsoba::umetniRed(int red, Osoba *element)
{
    osobe.insert(osobe.begin() + red, element);
    ApstraktniTabelarniModel::umetniRed(red, element);
}

void KolekcijaOsoba::ukloniRed(int indeks)
{
    osobe.erase(osobe.begin() + indeks);
    ApstraktniTabelarniModel::ukloniRed(indeks);
}

void KolekcijaOsoba::zapisi(ostream &output)
{
    output << osobe.size() << endl;
    for (int i = 0; i < osobe.size(); i++)
    {
        output << osobe.at(i) << endl;
    }
}
void KolekcijaOsoba::procitaj(istream &input)
{
    int brojOsoba;
    input >> brojOsoba;
    string tip;
    for (int i = 0; i < brojOsoba; i++)
    {
        Osoba *novaOsoba;
        input >> tip;
        if (tip == "osoba")
        {
            novaOsoba = new Osoba();
        }
        else if (tip == "radnik")
        {
            novaOsoba = new Radnik();
        }
        else if (tip == "vozac")
        {
            novaOsoba = new Vozac();
        }
        else if (tip == "komercijalista")
        {
            novaOsoba = new Komercijalista();
        }
        input >> novaOsoba;
        umetniElement(osobe.size(), novaOsoba);
    }
}

KolekcijaOsoba::~KolekcijaOsoba()
{
}