#include "PrikazOsoba.hpp"

PrikazOsoba::PrikazOsoba(int x, int y,
                         int w, int h,
                         ApstraktniModel<Osoba *> *model) : Fl_Group(x, y, w, h),
                                                            model(model), trenutni(-1)
{
    imeOutput = new Fl_Output(x + 50, y + 0, 200, 30);
    prezimeOutput = new Fl_Output(x + 50, y + 40, 200, 30);

    prethodniButton = new Fl_Button(x, y, 45, 70, "@<-");
    sledeciButton = new Fl_Button(x + 255, y, 45, 70, "@->");

    prethodniButton->callback(predjiNaPrethodni, this);
    sledeciButton->callback(predjiNaSledeci, this);

    model->pretplatiSlusaoca(this);

    this->end();
}

void PrikazOsoba::azurirajLabelu()
{
    stringstream sstream;
    sstream << "Osoba " << trenutni + 1 << "/" << model->brojElemenata();
    this->copy_label(sstream.str().c_str());
}

void PrikazOsoba::proveraDugmica()
{
    if (trenutni <= 0)
    {
        prethodniButton->deactivate();
    }
    else
    {
        prethodniButton->activate();
    }
    if (trenutni >= model->brojElemenata() - 1)
    {
        sledeciButton->deactivate();
    }
    else
    {
        sledeciButton->activate();
    }
}

void PrikazOsoba::postaviPrikaz(int indeks)
{
    if (indeks >= 0 && indeks < model->brojElemenata())
    {
        trenutni = indeks;
        imeOutput->value(model->dobaviElement(indeks)->getIme().c_str());
        prezimeOutput->value(model->dobaviElement(indeks)->getPrezime().c_str());
    }
    azurirajLabelu();
}

void PrikazOsoba::predjiNaSledeci(Fl_Widget *widget, void *data)
{
    PrikazOsoba *prikazOsoba = (PrikazOsoba *)data;
    if (prikazOsoba->trenutni + 1 < prikazOsoba->model->brojElemenata())
    {
        prikazOsoba->trenutni++;
        prikazOsoba->postaviPrikaz(prikazOsoba->trenutni);
    }

    prikazOsoba->proveraDugmica();
}

void PrikazOsoba::predjiNaPrethodni(Fl_Widget *widget, void *data)
{
    PrikazOsoba *prikazOsoba = (PrikazOsoba *)data;
    if (prikazOsoba->trenutni - 1 >= 0)
    {
        prikazOsoba->trenutni--;
        prikazOsoba->postaviPrikaz(prikazOsoba->trenutni);
    }

    prikazOsoba->proveraDugmica();
}

void PrikazOsoba::elementUmetnut(int indeks, Osoba *element) {
    if(trenutni == -1) {
        trenutni = 0;
        postaviPrikaz(trenutni);
    }
    proveraDugmica();
    azurirajLabelu();
}
void PrikazOsoba::elementUklonjen(int indeks) {
    proveraDugmica();
    azurirajLabelu();
}

PrikazOsoba::~PrikazOsoba()
{
}