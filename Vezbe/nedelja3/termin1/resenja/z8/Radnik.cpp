#include "Radnik.hpp"

Radnik::Radnik() : Osoba()
{
}
Radnik::Radnik(string ime, string prezime, double visinaPlate) : Osoba(ime, prezime), visinaPlate(visinaPlate)
{
}
double Radnik::getVisinaPlate()
{
    return visinaPlate;
}
void Radnik::setVisinaPLate(double visinaPlate)
{
    this->visinaPlate = visinaPlate;
}

string Radnik::getTip()
{
    return "radnik";
}
void Radnik::zapisi(ostream &output)
{
    Osoba::zapisi(output);
    output << " " << visinaPlate;
}

void Radnik::procitaj(istream &input)
{
    Osoba::procitaj(input);
    input >> visinaPlate;
}

Radnik::~Radnik()
{
}