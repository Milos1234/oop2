/**
 * Proširiti prethodni zadatak dodavanjem
 * dijaloga sa upitom za čuvanje izmena.
 */

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/fl_ask.H>

#include "FormaOsoba.hpp"
#include "PrikazOsoba.hpp"
#include "BudzetPrikaz.hpp"
#include "TabelaOsoba.hpp"

#include "KolekcijaOsoba.hpp"

using namespace std;

ostream &operator<<(ostream &output, Osoba *osoba)
{
    osoba->zapisi(output);
    return output;
}

istream &operator>>(istream &input, Osoba *osoba)
{
    osoba->procitaj(input);
    return input;
}

ostream &operator<<(ostream &output, KolekcijaOsoba &osobe)
{
    osobe.zapisi(output);
    return output;
}

istream &operator>>(istream &input, KolekcijaOsoba &osobe)
{
    osobe.procitaj(input);
    return input;
}

ostream &operator<<(ostream &output, KolekcijaOsoba *osobe)
{
    osobe->zapisi(output);
    return output;
}

istream &operator>>(istream &input, KolekcijaOsoba *osobe)
{
    osobe->procitaj(input);
    return input;
}

void cuvanjeIzmena(Fl_Widget *widget, void *data)
{
    int odgovor = fl_choice("Да ли желите да сачувате измене и напустите програм?", "Da", "Ne", "Odustani");
    if (odgovor == 0)
    {
        KolekcijaOsoba *osobe = (KolekcijaOsoba *)data;
        ofstream datoteka("osobe.txt");
        datoteka << osobe;
        datoteka.close();
    }
    if (odgovor < 2)
    {
        widget->hide();
    }
}

int main()
{
    KolekcijaOsoba osobe;

    Fl_Window *window = new Fl_Window(800, 600, "Zadatak 5");
    FormaOsoba *formaOsoba = new FormaOsoba(0, 50, 300, 240, "Podaci o osobi", &osobe);
    PrikazOsoba *prikazOsoba = new PrikazOsoba(400, 50, 300, 150, &osobe);
    BudzetPrikaz *budzetPrikaz = new BudzetPrikaz(400, 180, 300, 30, 120000, 40000);
    TabelaOsoba *tabelaOsoba = new TabelaOsoba(10, 310, 780, 280, &osobe);
    window->end();

    ifstream datoteka("osobe.txt");
    datoteka >> osobe;
    datoteka.close();
    window->callback(cuvanjeIzmena, &osobe);

    window->show();
    return Fl::run();
}