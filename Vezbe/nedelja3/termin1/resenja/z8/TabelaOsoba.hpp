#ifndef TABELA_OSOBA_HPP
#define TABELA_OSOBA_HPP

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Table_Row.H>

#include "ApstraktniTabelarniModel.hpp"
#include "ApstraktniPrikaz.hpp"
#include "Osoba.hpp"

class TabelaOsoba : public Fl_Table_Row, protected ApstraktniPrikaz<Osoba *>
{
protected:
    ApstraktniTabelarniModel<Osoba *> *model;
    virtual void draw_cell(TableContext context, int red = 0, int kolona = 0, int x = 0, int y = 0, int w = 0, int h = 0);
    virtual void elementUmetnut(int red, Osoba* element);
    virtual void elementUklonjen(int red);

public:
    TabelaOsoba(int x, int y, int w, int h, ApstraktniTabelarniModel<Osoba *> *model);
    virtual ~TabelaOsoba();
};

#endif