#ifndef KOLEKCIJA_OSOBA_HPP
#define KOLEKCIJA_OSOBA_HPP

#include <vector>
#include <sstream>
#include "ApstraktniTabelarniModel.hpp"
#include "Osoba.hpp"
#include "Radnik.hpp"
#include "Vozac.hpp"
#include "Komercijalista.hpp"

using namespace std;

class KolekcijaOsoba : public ApstraktniTabelarniModel<Osoba *>
{
private:
    vector<Osoba *> osobe;

public:
    KolekcijaOsoba();
    virtual int brojRedova();
    virtual int brojKolona();
    virtual Osoba* dobaviRed(int red);
    virtual string dobaviTekst(int red, int kolona);
    virtual string horizontalnoZaglavlje(int kolona);
    virtual string vertikalnoZaglavlje(int red);
    virtual void umetniRed(int red, Osoba* element);
    virtual void ukloniRed(int red);
    void zapisi(ostream &output);
    void procitaj(istream &input);
    virtual ~KolekcijaOsoba();

    friend ostream &operator<<(ostream &output, KolekcijaOsoba &kolekcijaOsoba);
    friend istream &operator>>(istream &input, KolekcijaOsoba &kolekcijaOsoba);
    friend ostream &operator<<(ostream &output, KolekcijaOsoba *kolekcijaOsoba);
    friend istream &operator>>(istream &input, KolekcijaOsoba *kolekcijaOsoba);
};

#endif