#ifndef KOMERCIJALISTA_HPP
#define KOMERCIJALISTA_HPP

#include "Radnik.hpp"

class Komercijalista : public Radnik
{
private:
    string odeljenje;

public:
    Komercijalista();
    Komercijalista(string ime, string prezime, double visinaPlate, string odeljenje);
    string getOdeljenje();
    void setOdeljenje(string odeljenje);
    virtual string getTip();
    virtual void zapisi(ostream &output);
    virtual void procitaj(istream &input);
    virtual ~Komercijalista();
};

#endif