#ifndef APSTRAKTNI_TABELARNI_MODEL
#define APSTRAKTNI_TABELARNI_MODEL

#include <string>

#include "ApstraktniModel.hpp"

using namespace std;

template <typename T>
class ApstraktniTabelarniModel : public ApstraktniModel<T>
{
public:
    virtual int brojElemenata();
    virtual T dobaviElement(int red);
    virtual void umetniElement(int red, T element);
    virtual void ukloniElement(int red);
    virtual int brojRedova() = 0;
    virtual int brojKolona() = 0;
    virtual T dobaviRed(int red) = 0;
    virtual string dobaviTekst(int red, int kolona) = 0;
    virtual string horizontalnoZaglavlje(int kolona) = 0;
    virtual string vertikalnoZaglavlje(int red) = 0;
    virtual void umetniRed(int red, T element) = 0;
    virtual void ukloniRed(int red) = 0;
    virtual ~ApstraktniTabelarniModel();
};

template <typename T>
int ApstraktniTabelarniModel<T>::brojElemenata()
{
    return brojRedova();
}

template <typename T>
T ApstraktniTabelarniModel<T>::dobaviElement(int red)
{
    return dobaviRed(red);
}

template <typename T>
void ApstraktniTabelarniModel<T>::umetniElement(int red, T element)
{
    umetniRed(red, element);
}

template <typename T>
void ApstraktniTabelarniModel<T>::ukloniElement(int red)
{
    ukloniRed(red);
}

template <typename T>
void ApstraktniTabelarniModel<T>::umetniRed(int red, T element)
{
    ApstraktniModel<T>::umetniElement(red, element);
}

template <typename T>
void ApstraktniTabelarniModel<T>::ukloniRed(int red)
{
    ApstraktniModel<T>::ukloniElement(red);
}

template <typename T>
ApstraktniTabelarniModel<T>::~ApstraktniTabelarniModel()
{
}

#endif