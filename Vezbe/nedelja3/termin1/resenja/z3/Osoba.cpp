#include "Osoba.hpp"

Osoba::Osoba() {}
Osoba::Osoba(string ime, string prezime) : ime(ime), prezime(prezime) {}

string Osoba::getIme()
{
    return ime;
}

void Osoba::setIme(string ime)
{
    this->ime = ime;
}
string Osoba::getPrezime()
{
    return prezime;
}
void Osoba::setPrezime(string prezime)
{
    this->prezime = prezime;
}

Osoba::~Osoba()
{
}