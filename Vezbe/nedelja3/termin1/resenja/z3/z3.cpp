/**
 * Prepraviti prethodni zadatak tako da se
 * pri dodavanje nove osobe vrši zapisivanje u
 * datoteku. A pri pokretanju programa čitanje
 * podataka o osobama iz datoteke.
 */

#include <FL/Fl.H>
#include <FL/Fl_Window.H>

#include "FormaOsoba.hpp"
#include "PrikazOsoba.hpp"
#include "Osoba.hpp"

using namespace std;

ostream &operator<<(ostream &output, Osoba &osoba)
{
    output << osoba.getIme() << " " << osoba.getPrezime();
    return output;
}

istream &operator>>(istream &input, Osoba &osoba)
{
    string ime;
    string prezime;
    input >> ime >> prezime;
    osoba.setIme(ime);
    osoba.setPrezime(prezime);
    return input;
}

ostream &operator<<(ostream &output, vector<Osoba> &osobe)
{
    output << osobe.size() << endl;
    for (int i = 0; i < osobe.size(); i++)
    {
        output << osobe.at(i) << endl;
    }
    return output;
}

istream &operator>>(istream &input, vector<Osoba> &osobe)
{
    int brojOsoba;
    input >> brojOsoba;
    for (int i = 0; i < brojOsoba; i++)
    {
        Osoba novaOsoba;
        input >> novaOsoba;
        osobe.push_back(novaOsoba);
    }
    return input;
}

int main()
{
    vector<Osoba> osobe;

    ifstream file("osobe.txt");
    file >> osobe;
    file.close();

    Fl_Window *window = new Fl_Window(800, 600, "Zadatak 3");
    FormaOsoba *formaOsoba = new FormaOsoba(0, 50, 300, 150, "Podaci o osobi", &osobe);
    PrikazOsoba *prikazOsoba = new PrikazOsoba(400, 50, 300, 150, &osobe);

    window->end();
    window->show();
    return Fl::run();
}