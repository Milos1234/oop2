#include "FormaOsoba.hpp"

#include <iostream>

using namespace std;

FormaOsoba::FormaOsoba(int x, int y,
                       int width, int height,
                       const char *label,
                       vector<Osoba> *osobe) : Fl_Group(x, y, width, height, label),
                                               osobe(osobe)
{
    imeInput = new Fl_Input(x + 100, y + 10, 200, 30, "Ime:");
    prezimeInput = new Fl_Input(x + 100, y + 50, 200, 30, "Prezime:");
    dodaj = new Fl_Button(x + 100, y + 90, 100, 30, "Dodaj");
    dodaj->callback(FormaOsoba::dodajOsobu, this);
    this->end();
}

void FormaOsoba::dodajOsobu(Fl_Widget *widget, void *data)
{
    FormaOsoba *forma = (FormaOsoba *)data;
    Osoba novaOsoba(forma->imeInput->value(),
                    forma->prezimeInput->value());
    forma->osobe->push_back(novaOsoba);
    ofstream out("osobe.txt");
    out << *(forma->osobe);
    out.close();
}

FormaOsoba::~FormaOsoba()
{
}