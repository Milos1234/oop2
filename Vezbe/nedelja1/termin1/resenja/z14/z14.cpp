/**
 * Klasu PrevoznoSredstvo proširiti dodavanjem reference na vozača koji duži prevozno sredstvo.
 * U klasi PrevoznoSredstvo napisati metodu za ispis podatataka o prevoznom sredstvu i vozaču.
 * Dodati get i set metode za dobavljanje vozača.
 */

#include <iostream>
#include <string>

class Vozac
{
private:
    std::string ime;
    std::string prezime;
    std::string kategorija;

public:
    Vozac(std::string ime, std::string prezime, std::string kategorija) : ime(ime), prezime(prezime),
                                                                          kategorija(kategorija)
    {
    }
    void detalji()
    {
        std::cout << ime << " " << prezime << ", ima dozvolu za kategoriju " << kategorija;
    }
};

class PrevoznoSredstvo
{
protected:
    std::string proizvodjac;
    std::string model;
    Vozac &vozac;

public:
    PrevoznoSredstvo(std::string proizvodjac, std::string model, Vozac &vozac) : proizvodjac(proizvodjac),
                                                                                 model(model), vozac(vozac) {}
    Vozac &getVozac()
    {
        return vozac;
    }
    void setVozac(Vozac &vozac)
    {
        this->vozac = vozac;
    }
    void detalji()
    {
        std::cout << proizvodjac << " " << model << ", vozac: ";
        vozac.detalji();
    }
};

int main()
{
    //Test ispravnosti resenja.
    int ispravniKoraci = 0;
    Vozac vozac1("Ime1", "Prezime1", "A");
    Vozac vozac2("Ime2", "Prezime2", "B");
    PrevoznoSredstvo ps("Proizvodjac", "Model", vozac1);
    if (&ps.getVozac() == &vozac1)
    {
        ispravniKoraci++;
    }
    ps.setVozac(vozac2);
    if (&ps.getVozac() == &vozac1)
    {
        ispravniKoraci++;
    }
    if (ispravniKoraci == 2)
    {
        ps.detalji();
        std::cout << std::endl
                  << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.
    return 0;
}