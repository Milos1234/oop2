/**
 * Napisati klasu Preduzeće sa atributima naziv, adresa, brojRadnika i mesecniTroskovi.
 * Atributi naziv i adresa su tipa string, brojRadnika je tipa int a mesecniTroskovi tipa double.
 * Svi atributi imaju private vidljivost.
 * Napisati podrazumevani konstruktor, konstruktor sa parametrima i get i set metode.
 * Konstruktor sa parametrima prima sve paranetre neophodne za postavljanje vrednosti atributa.
 * Napisati metodu troskoviPoRadniku() koja računa troškove po radniku po formuli mesecniTroskovi/brojRadnika.
 * Konstruktori, get i set metode i metoda troskoviPoRadniku() su javno vidljivi.
 */

#include <iostream>
#include <string>

class Preduzece
{
private:
    std::string naziv;
    std::string adresa;
    int brojRadnika;
    double mesecniTroskovi;

public:
    Preduzece() {}
    Preduzece(std::string naziv, std::string adresa, int brojRadnika, double mesecniTroskovi)
    {
        this->naziv = naziv;
        this->adresa = adresa;
        this->brojRadnika = brojRadnika;
        this->mesecniTroskovi = mesecniTroskovi;
    }

    std::string getNaziv()
    {
        return this->naziv;
    }

    void setNaziv(std::string naziv)
    {
        this->naziv = naziv;
    }

    std::string getAdresa()
    {
        return this->adresa;
    }

    void setAdresa(std::string adresa)
    {
        this->adresa = adresa;
    }

    int getBrojRadnika()
    {
        return this->brojRadnika;
    }

    void setBrojRadnika(int brojRadnika)
    {
        this->brojRadnika = brojRadnika;
    }

    double getMesecniTroskovi()
    {
        return this->mesecniTroskovi;
    }

    void setMesecniTroskovi(double mesecniTroskovi)
    {
        this->mesecniTroskovi = mesecniTroskovi;
    }

    double troskoviPoRadniku()
    {
        return this->mesecniTroskovi / this->brojRadnika;
    }
};

int main()
{
    //Test ispravnosti resenja.
    Preduzece testPreduzece1;
    testPreduzece1.setNaziv("naziv1");
    testPreduzece1.setAdresa("adresa1");
    testPreduzece1.setBrojRadnika(10);
    testPreduzece1.setMesecniTroskovi(100);
    Preduzece testPreduzece2("naziv2", "adresa2", 10, 200);

    bool ispravnoTestPreduzece1 = false;
    bool ispravnoTestPReduzece2 = false;

    if (testPreduzece1.getNaziv() == "naziv1" &&
        testPreduzece1.getAdresa() == "adresa1" &&
        testPreduzece1.getBrojRadnika() == 10 &&
        testPreduzece1.getMesecniTroskovi() == 100 &&
        testPreduzece1.troskoviPoRadniku() == 10)
    {
        ispravnoTestPreduzece1 = true;
    }

    if (testPreduzece2.getNaziv() == "naziv2" &&
        testPreduzece2.getAdresa() == "adresa2" &&
        testPreduzece2.getBrojRadnika() == 10 &&
        testPreduzece2.getMesecniTroskovi() == 200 &&
        testPreduzece2.troskoviPoRadniku() == 20)
    {
        ispravnoTestPReduzece2 = true;
    }

    if (ispravnoTestPreduzece1 && ispravnoTestPReduzece2)
    {
        std::cout << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.

    return 0;
}