/**
 * Po zadatom upustvu podesiti razvojno okruženje, pokrenuti program u debug režimu i ispratiti izvršavanje programa.
 */

#include <iostream>
#include <string>

class Radnik
{
private:
    std::string ime;
    std::string prezime;
    double plata;

public:
    std::string getIme()
    {
        return ime;
    }
    void setIme(std::string ime)
    {
        this->ime = ime;
    }
    std::string getPrezime()
    {
        return prezime;
    }
    void setPrezime(std::string prezime)
    {
        this->prezime = prezime;
    }
    double getPlata()
    {
        return plata;
    }
    void setPlata(double plata)
    {
        this->plata = plata;
    }
};

int main()
{
    Radnik radnik;
    std::string ime;
    std::string prezime;
    double plata;
    std::cout << "Unesite ime radnika: ";
    std::cin >> ime; // Ovde postaviti prvu tačku prekida.
    std::cout << "Unesite prezime radnika: ";
    std::cin >> prezime;
    std::cout << "Unesite platu radnika: ";
    std::cin >> plata;

    radnik.setIme(ime); // Ovde postaviti drugu tačku prekida.
    radnik.setPrezime(prezime);
    radnik.setPlata(plata);
    std::cout << "Radnik: " << radnik.getIme() << " " << radnik.getPrezime() << ", visina plate: " << radnik.getPlata() << std::endl;
    return 0;
}