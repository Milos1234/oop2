/**
 * Napisati klasu PrevoznoSredstvo koja ima protected atribute
 * proizvođač, model, tipGoriva i potrošnja.
 * Dodati podrazumevani konstruktor i konstruktor sa parametrima.
 * Dodati get i set metode.
 * Konstruktori i metode su javno vidljivi.
 * 
 * Dodati klasu Kombi koja nasleđuje klasu prevozno sredstvo.
 * U ovu klasu dodati atribute kapacitet i popunjenost tipa double.
 * Pored podrazumevanog konstruktora i konstruktora sa parametrima dodati
 * metodu utovar(kolicina) koja, ukoliko ima mesta, povećava popunjenost
 * za zadatu kolicinu i vraća true a u suprotnom ispisuje poruku
 * "Nema prostora u vozilu." i vraća false.
 */
#include <iostream>
#include <string>

class PrevoznoSredstvo
{
protected:
    std::string proizvodjac;
    std::string model;
    std::string tipGoriva;
    double potrosnja;

public:
    PrevoznoSredstvo() {}
    PrevoznoSredstvo(std::string proizvodjac, std::string model,
                     std::string tipGoriva, double potrosnja) : proizvodjac(proizvodjac),
                                                                model(model),
                                                                tipGoriva(tipGoriva),
                                                                potrosnja(potrosnja) {}
    std::string getProizvodjac()
    {
        return proizvodjac;
    }
    void setProizvodjac(std::string proizvodjac)
    {
        this->proizvodjac = proizvodjac;
    }
    std::string getModel()
    {
        return model;
    }
    void setModel(std::string model)
    {
        this->model = model;
    }
    std::string getTipGoriva()
    {
        return tipGoriva;
    }
    void setTipGoriva(std::string tipGoriva)
    {
        this->tipGoriva = tipGoriva;
    }
    double getPotrosnja()
    {
        return potrosnja;
    }
    void setPotrosnja(double potrosnja)
    {
        this->potrosnja = potrosnja;
    }
};

class Kombi : public PrevoznoSredstvo
{
private:
    double kapacitet;
    double popunjenost;

public:
    Kombi() : PrevoznoSredstvo() {}
    Kombi(std::string proizvodjac, std::string model,
          std::string tipGoriva, double potrosnja,
          double kapacitet, double popunjenost = 0) : PrevoznoSredstvo(proizvodjac, model, tipGoriva, potrosnja),
                                                      kapacitet(kapacitet),
                                                      popunjenost(popunjenost) {}
    bool utovar(double kolicina)
    {
        if (popunjenost + kolicina <= kapacitet)
        {
            popunjenost += kolicina;
            return true;
        }
        std::cout << "Nema prostora u vozilu." << std::endl;
        return false;
    }
};

int main()
{
    //Test ispravnosti resenja.
    int ispravniKoraci = 0;
    PrevoznoSredstvo prevoznosSredstvo("Proizvodjac", "Model", "Benzin", 6.2);
    if (prevoznosSredstvo.getProizvodjac() == "Proizvodjac" &&
        prevoznosSredstvo.getModel() == "Model" &&
        prevoznosSredstvo.getTipGoriva() == "Benzin" &&
        prevoznosSredstvo.getPotrosnja() == 6.2)
    {
        ispravniKoraci++;
    }

    prevoznosSredstvo.setProizvodjac("p");
    prevoznosSredstvo.setModel("m");
    prevoznosSredstvo.setTipGoriva("g");
    prevoznosSredstvo.setPotrosnja(1);

    if (prevoznosSredstvo.getProizvodjac() == "p" &&
        prevoznosSredstvo.getModel() == "m" &&
        prevoznosSredstvo.getTipGoriva() == "g" &&
        prevoznosSredstvo.getPotrosnja() == 1)
    {
        ispravniKoraci++;
    }
    Kombi kombi("Proizvodjac2", "Model2", "Dizel", 7.3, 10, 1);
    if (kombi.utovar(1))
    {
        ispravniKoraci++;
    }
    if (!kombi.utovar(100))
    {
        ispravniKoraci++;
    }

    if (ispravniKoraci == 4)
    {
        std::cout << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.
    return 0;
}