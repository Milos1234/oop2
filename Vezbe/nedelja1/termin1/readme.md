# Uvod

Ciljevi uvodinih vežbi su podešavanje radnog okruženja i upoznavanje sa osnovnim konceptima programskog jezika C++. Na ovim vežbama će biti obrađene sledeće teme:

1. Podešavanje razvojnog okruženja
2. Primer jednostavnog C++ programa
3. Pronalaženje i otklanjanje grešaka
4. Jednostavni primeri nasleđivanja
5. Reference

## Podešavanja razvojnog okruženja

Potrebno je preuzeti sledeće softverske pakete:

1. [Git](https://git-scm.com)
2. [Mingw-w64](http://mingw-w64.org/doku.php)
3. [Visual Studio Code](https://code.visualstudio.com/)

### Instalacija Git softvera za upravljanje verzijama

1. Preuzeti instalaciju sa [linka](https://git-scm.com)
2. Pokrenuti instalaciju i ispratiti korake prikazane na slikama

![](materijali/slike/git1.png)

![](materijali/slike/git2.png)

![](materijali/slike/git3.png)

![](materijali/slike/git4.png)

![](materijali/slike/git5.png)

![](materijali/slike/git6.png)

![](materijali/slike/git7.png)

![](materijali/slike/git8.png)

![](materijali/slike/git9.png)

![](materijali/slike/git10.png)

![](materijali/slike/git11.png)

![](materijali/slike/git12.png)

![](materijali/slike/git13.png)

![](materijali/slike/git14.png)

3. Otvoriti Git bash i klonirati repozitorijum predmeta

```bash
git clone https://gitlab.com/ivanradosavljevic/singi-oop2-2020.git
```
![](materijali/slike/git15.png)

![](materijali/slike/git16.png)

4. Pozicionirati se u klonirani repozitorijum i povući izmene

```bash
git pull
```

### Instalacija kompajlera

1. Preuzeti instalaciju sa [linka](http://mingw-w64.org/doku.php/download/mingw-builds)
2. Pokrenuti instalaciju i ispratiti korake prikazane na slikama

![](materijali/slike/mingw1.png)

![](materijali/slike/mingw2.png)

![](materijali/slike/mingw3.png)

![](materijali/slike/mingw4.png)

![](materijali/slike/mingw5.png)

![](materijali/slike/mingw6.png)

![](materijali/slike/mingw7.png)

![](materijali/slike/mingw8.png)

![](materijali/slike/mingw9.png)

![](materijali/slike/mingw10.png)

![](materijali/slike/mingw11.png)

![](materijali/slike/mingw12.png)

![](materijali/slike/mingw13.png)

![](materijali/slike/mingw14.png)

![](materijali/slike/mingw15.png)

### Instaliranje Visual Studio Code editora

1. Preuzeti instalaciju sa [linka](https://code.visualstudio.com/)
2. Pokrenuti instalaciju i ispratiti korake prikazane na slikama

![](materijali/slike/vscode1.png)

![](materijali/slike/vscode2.png)

![](materijali/slike/vscode3.png)

![](materijali/slike/vscode4.png)

![](materijali/slike/vscode5.png)

![](materijali/slike/vscode6.png)

![](materijali/slike/vscode7.png)

## Primeri jednostavnog C++ programa

1. Napraviti datoteku primer.cpp i popuniti je kodom iz listinga

```c++
#include <iostream>
#include <string>

int main() {
    std::string ime;
    std::cout << "Unesite ime: ";
    std::cin >> ime;
    std::cout << "Zdravo "<< ime << "!" << std::endl;
    return 0;
}
```

2. Otvoriti terminal na poziciji gde se nalazi datoteka i pokrenut kompajler

```bash
g++ primer.cpp
```

3. Pokrenuti dobijeni program

## Pronalaženje i otklanjanje grešaka

Za pronalaženje grešaka koristiće se program gdb koji dolazi u sklopu Mingw-w64 paketa. Pokretanje gdb-a je moguće direktno iz Visual Studio Code okruženja otvaranjem datoteke sa izvornim kodom i pritiskom na taster F5. Pre pokretanja gdb-a potrebno je definisati tačke prekida. Definisati tačku prekida klikom na traku pre rednog broja linije. Na zadatoj poziciji će se pojaviti crveni krug koji označava da je na datoj liniji postavljena tačka prekida.

----
## Dodatni materijali

* Knjiga za Git: [Pro Git](https://git-scm.com/book/en/v2)
* Interaktivni tutorijal za Git: https://learngitbranching.js.org/