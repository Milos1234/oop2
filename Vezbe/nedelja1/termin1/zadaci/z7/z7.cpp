/**
 * Proširiti klasu Vozac redefinisanjem metode brutoPlata().
 * Ova metoda računa bruto platu na osnovu kategorije vozača, ukoliko
 * vozač ima kategoriju C bruto plata mu se uvećava za 10000 dinara.
 */

#include <iostream>
#include <string>

class Radnik
{
private:
    std::string ime;
    std::string prezime;
    double plata;
    double osnovicaOsiguranja;

public:
    Radnik() {}
    Radnik(std::string ime, std::string prezime, double plata, double osnovicaOsiguranja)
    {
        this->ime = ime;
        this->prezime = prezime;
        this->plata = plata;
        this->osnovicaOsiguranja = osnovicaOsiguranja;
    }

    std::string getIme()
    {
        return ime;
    }
    void setIme(std::string ime)
    {
        this->ime = ime;
    }
    std::string getPrezime()
    {
        return prezime;
    }
    void setPrezime(std::string prezime)
    {
        this->prezime = prezime;
    }
    double getPlata()
    {
        return plata;
    }
    void setPlata(double plata)
    {
        this->plata = plata;
    }

    void detalji()
    {
        std::cout << "Radnik: " << ime << " " << prezime << ", visina plate: " << plata;
    }

    double brutoPlata()
    {
        return plata + (plata * osnovicaOsiguranja);
    }
};

class Vozac : public Radnik
{
private:
    std::string kategorija;

public:
    Vozac(std::string ime, std::string prezime, double plata,
          double osnovicaOsiguranja, std::string kategorija) : Radnik(ime, prezime, plata, osnovicaOsiguranja),
                                                               kategorija(kategorija)
    {
    }
    void detalji()
    {
        Radnik::detalji();
        std::cout << ", ima dozvolu za kategoriju " << kategorija;
    }
};

int main()
{
    //Test ispravnosti resenja.
    Vozac testVozacA("Jakov", "Djuric", 55000, 0.2, "A");
    Vozac testVozacC("Marko", "Petrovic", 60000, 0.2, "C");

    bool ispravnoTestVozacA = false;
    bool ispravnoTestVozacC = false;

    if (testVozacA.brutoPlata() == 55000 + 55000 * 0.2)
    {
        testVozacA.detalji();
        std::cout << std::endl;
        ispravnoTestVozacA = true;
    }

    if (testVozacC.brutoPlata() == 60000 + 60000 * 0.2 + 10000)
    {
        testVozacC.detalji();
        std::cout << std::endl;
        ispravnoTestVozacC = true;
    }

    if (ispravnoTestVozacA && ispravnoTestVozacC)
    {
        std::cout << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.

    return 0;
}