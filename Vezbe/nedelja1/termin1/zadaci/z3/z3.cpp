/**
 * Proširiti klasu Radnik uvođenjem javne metode detalji. Ova metoda ispisuje sve podatke o radniku.
 * Dodati get i set metode za sve atribute klase Radnik.
 */

#include <iostream>
#include <string>

class Radnik
{
private:
    std::string ime;
    std::string prezime;
    double plata;
    double osnovicaOsiguranja;

public:
    Radnik();
    Radnik(std::string ime, std::string prezime, double plata, double osnovicaOsiguranja)
    {
        this->ime = ime;
        this->prezime = prezime;
        this->plata = plata;
        this->osnovicaOsiguranja = osnovicaOsiguranja;
    }

    double brutoPlata()
    {
        return plata + (plata * osnovicaOsiguranja);
    }
};

int main()
{
    //Test ispravnosti resenja.
    Radnik testRadnik("Test", "Test", 1000, 0.1);
    if (testRadnik.brutoPlata() == 1000 + 1000 * 0.1)
    {
        testRadnik.detalji();
        std::cout << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.

    return 0;
}