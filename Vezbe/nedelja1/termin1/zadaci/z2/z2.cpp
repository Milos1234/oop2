/**
 * Napisati klasu Radnik sa atributima ime, prezeime, plata i osnovicaOsiguranja.
 * Ime i prezime su tipa string, plata i osnovica osiguranja su tipa double. Svi atributi su privatni.
 * Napisati konstruktor koji kao parametre prima vrednosti svih prethodno navedenih atributa.
 * Napisati metodu brutoPlata() koja računa bruto platu po formuli plata + (plata * osnovicaOsiguranja).
 * Metoda i konstruktor su javni.
 */

#include <iostream>
#include <string>

int main()
{
    //Primer instanciranja radnika.
    Radnik marko("Marko", "Petrovic", 36700, 0.15);
    std::cout << "Bruto plata: " << marko.brutoPlata() << std::endl;

    //Test ispravnosti resenja.
    Radnik testRadnik("Test", "Test", 1000, 0.1);
    if (testRadnik.brutoPlata() == 1000 + 1000 * 0.1)
    {
        std::cout << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.

    return 0;
}