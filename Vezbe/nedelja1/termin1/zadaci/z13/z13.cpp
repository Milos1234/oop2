/**
 * Proširit klasu dokument tako da ima referencu na autora dokumenta. Autor dokumenta je radnik.
 * Prepraviti metodu za štampanje tako da ispiše podatke o autoru.
 * Dodati get i set metode za autora dokumenta.
 */

#include <iostream>
#include <string>

class Radnik
{
private:
    std::string ime;
    std::string prezime;
    double plata;
    double osnovicaOsiguranja;

public:
    Radnik() {}
    Radnik(std::string ime, std::string prezime, double plata, double osnovicaOsiguranja)
    {
        this->ime = ime;
        this->prezime = prezime;
        this->plata = plata;
        this->osnovicaOsiguranja = osnovicaOsiguranja;
    }

    std::string getIme()
    {
        return ime;
    }
    void setIme(std::string ime)
    {
        this->ime = ime;
    }
    std::string getPrezime()
    {
        return prezime;
    }
    void setPrezime(std::string prezime)
    {
        this->prezime = prezime;
    }
    double getPlata()
    {
        return plata;
    }
    void setPlata(double plata)
    {
        this->plata = plata;
    }

    void detalji()
    {
        std::cout << "Radnik: " << ime << " " << prezime << ", visina plate: " << plata;
    }

    double brutoPlata()
    {
        return plata + (plata * osnovicaOsiguranja);
    }
};

class Dokument
{
protected:
    std::string naslov;

public:
    Dokument(std::string naslov) : naslov(naslov) {}
    void stampaj()
    {
        std::cout << naslov << " ";
    }
};

int main()
{
    //Test ispravnosti resenja.
    int ispravniKoraci = 0;
    Radnik radnik1("Ime", "Prezime", 2000, 0.1);
    Radnik radnik2("Ime", "Prezime", 1000, 0.1);
    Dokument d("Naslov", radnik1);
    if (&d.getAutor() == &radnik1)
    {
        ispravniKoraci++;
    }
    d.setAutor(radnik2);
    if (&d.getAutor() == &radnik1)
    {
        ispravniKoraci++;
    }
    if (ispravniKoraci == 2)
    {
        d.stampaj();
        std::cout << std::endl
                  << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.
    return 0;
}