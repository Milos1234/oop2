/**
 * Napisati klasu arhiva koja sadrži vektor arhiviranih dokumenata.
 * Pored podrazumevanog konstruktora ova klasa poseduje metodu dodajDokument(dokument)
 * i pretraga(naslov). Metoda dodajDokument dodaje novi dokument u arhivu.
 * Metoda pretraga vrši pretragu dokuemnta po naslov i ukoliko ga pronađe vraća
 * pronađeni dokument, u suprotnom vraća dokument konstruisan podrazumevanim konstruktorom.
 */

#include <iostream>

#include "Dokument.hpp"

int main()
{
    Dokument d1("Dokument 1", "test");
    Dokument d2("dokument 2", "test2");
    Dokument d3("dokument 3", "test 3");
    Arhiva a;
    a.dodajDokument(d1);
    a.dodajDokument(d2);
    a.dodajDokument(d3);
    a.pronadjiDokument("dokument").stampaj();
    a.pronadjiDokument("dokument 2").stampaj();

    return 0;
}