#include "Radnik.hpp"

Radnik::Radnik() {}
Radnik::Radnik(std::string ime, std::string prezime,
               double plata, double osnovicaOsiguranja) : ime(ime), prezime(prezime),
                                                          plata(plata), osnovicaOsiguranja(osnovicaOsiguranja)
{
}

double Radnik::brutoPlata()
{
    return plata + (plata * osnovicaOsiguranja);
}