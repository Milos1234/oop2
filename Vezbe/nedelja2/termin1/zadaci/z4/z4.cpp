/**
 * Napisati klasu PravniDokument koja naslеđuje klasu Dokument i
 * uvodi atribut preambula koji je pokazivač na dokument.
 * Redefinisati metodu ispis tako da se, ukoliko postoji, prvo
 * ispiše preambula a potom sadržaj pravnog dokumenta.
 */

#include <iostream>
#include <string>

int main()
{
    //Test ispravnosti resenja.
    Dokument *preambula1 = new Dokument("Preambula1", "Autor1");
    PravniDokument *preambula2 = new PravniDokument("Preambula2", "Autor2", preambula1);
    PravniDokument *pravniDokument = new PravniDokument("Naslov3", "Autor3", preambula2);
    pravniDokument->stampaj();
    //Kraj testa.
    return 0;
}