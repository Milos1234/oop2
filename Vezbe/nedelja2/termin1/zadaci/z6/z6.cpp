/**
 * Proširiti klasu Preduzece tako da sadrži spisak svih zaposlenih radnika.
 * U klasu Preduzece dodati metodu zaposliRadnika koja prima referencu na radnika kojeg treba zaposliti.
 * U klasu Preduzece dodati metodu spisakZaposlenih koja ispisuje redni broj i detalje zaposlenih.
 * Prepraviti metodu getMesecniTroskovi tako da se troškovi računaju kao suma bruto plata svih zaposlenih.
 */

#include <iostream>

#include "Radnik.hpp"
#include "Preduzece.hpp"

int main()
{
    //Test ispravnosti resenja.
    Preduzece preduzece("Naziv", "Adresa");
    Radnik r1("Ime", "Prezime", 45000, 0.1);
    Radnik r2("Ime", "Prezime", 54300, 0.1);

    preduzece.zaposliRadnika(r1);
    preduzece.zaposliRadnika(r2);
    preduzece.spisakZaposlenih();

    std::cout << preduzece.getNaziv() << std::endl;
    std::cout << preduzece.getMesecniTroskovi() << std::endl;

    if (std::abs(preduzece.getMesecniTroskovi() - (r1.brutoPlata() + r2.brutoPlata())) < 0.1e-10)
    {
        std::cout << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.
    return 0;
}