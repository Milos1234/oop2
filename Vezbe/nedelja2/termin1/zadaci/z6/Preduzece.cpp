#include "Preduzece.hpp"

Preduzece::Preduzece()
{
}

Preduzece::Preduzece(std::string naziv, std::string adresa) : naziv(naziv), adresa(adresa)
{
}

std::string Preduzece::getNaziv()
{
    return this->naziv;
}

void Preduzece::setNaziv(std::string naziv)
{
    this->naziv = naziv;
}

std::string Preduzece::getAdresa()
{
    return this->adresa;
}

void Preduzece::setAdresa(std::string adresa)
{
    this->adresa = adresa;
}

int Preduzece::getBrojRadnika()
{
    return 0;
}
double Preduzece::getMesecniTroskovi()
{
    return 0;
}
