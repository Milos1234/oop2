#ifndef PREDUZECE_HPP
#define PREDUZECE_HPP

#include <iostream>
#include <string>
#include <vector>

class Preduzece
{
private:
    std::string naziv;
    std::string adresa;

public:
    Preduzece();
    Preduzece(std::string naziv, std::string adresa);
    std::string getNaziv();
    void setNaziv(std::string naziv);
    std::string getAdresa();
    void setAdresa(std::string adresa);
    int getBrojRadnika();
    double getMesecniTroskovi();
};

#endif