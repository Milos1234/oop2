#ifndef PREHRAMBENI_PROIZVOD_HPP
#define PREHRAMBENI_PROIZVOD_HPP

#include <iostream>
#include <string>

class PrehrambeniProizvod
{
protected:
    std::string naziv;
    std::string opis;
    double cena;

public:
    PrehrambeniProizvod();
    PrehrambeniProizvod(std::string naziv, std::string opis, double cena);
    std::string getNaziv();
    void setNaziv(std::string naziv);
    std::string getOpis();
    void setOpis(std::string opis);
    double getCena();
    void setCena(double cena);
    void detalji();
};

#endif