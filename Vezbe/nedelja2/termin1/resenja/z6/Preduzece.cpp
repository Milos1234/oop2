#include "Preduzece.hpp"

Preduzece::Preduzece() {}
Preduzece::Preduzece(std::string naziv, std::string adresa) : naziv(naziv), adresa(adresa) {}

std::string Preduzece::getNaziv()
{
    return this->naziv;
}

void Preduzece::setNaziv(std::string naziv)
{
    this->naziv = naziv;
}

std::string Preduzece::getAdresa()
{
    return this->adresa;
}

void Preduzece::setAdresa(std::string adresa)
{
    this->adresa = adresa;
}

int Preduzece::getBrojRadnika()
{
    return this->radnici.size();
}

void Preduzece::zaposliRadnika(Radnik &radnik)
{
    this->radnici.push_back(radnik);
}

double Preduzece::getMesecniTroskovi()
{
    double troskovi = 0;
    for (size_t i = 0; i < radnici.size(); i++)
    {
        troskovi += radnici[i].brutoPlata();
    }
    return troskovi;
}

void Preduzece::spisakZaposlenih()
{
    for (size_t i = 0; i < radnici.size(); i++)
    {
        std::cout << (i + 1) << ".\t";
        radnici[i].detalji();
    }
}