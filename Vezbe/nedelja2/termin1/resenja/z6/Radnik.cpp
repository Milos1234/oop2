#include "Radnik.hpp"

Radnik::Radnik() {}
Radnik::Radnik(std::string ime, std::string prezime, double plata,
               double osnovicaOsiguranja) : ime(ime), prezime(prezime),
                                            plata(plata),
                                            osnovicaOsiguranja(osnovicaOsiguranja) {}
double Radnik::getPlata()
{
    return plata;
}
void Radnik::setPlata(double plata)
{
    this->plata = plata;
}

void Radnik::detalji()
{
    std::cout << "Radnik: " << ime << " " << prezime << ", visina plate: " << plata << std::endl;
}

double Radnik::brutoPlata()
{
    return plata + (plata * osnovicaOsiguranja);
}