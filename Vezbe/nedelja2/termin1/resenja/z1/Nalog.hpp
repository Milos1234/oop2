#ifndef NALOG_HPP
#define NALOG_HPP

#include <iostream>
#include <string>

using namespace std;

namespace nalog
{
    class Nalog
    {
    private:
    string nalogodavac;
    string sadrzaj;
    protected:
    public:
    Nalog() {}
    Nalog(string nalogodavac, string sadrzaj) : nalogodavac(nalogodavac), sadrzaj(sadrzaj) {}
    virtual void stampaj() {
        cout << nalogodavac << " " << sadrzaj << endl;
    }
    };
} // namespace nalog
#endif