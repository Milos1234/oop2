#pragma once

#include <iostream>
#include <string>

#include "Nalog.hpp"

namespace nalog
{
    class RadniNalog : public Nalog
    {
    private:
        string angazovaniRadnici;

    protected:
    public:
        RadniNalog() : Nalog() {}
        RadniNalog(string nalogodavac, string sadrzaj,
                   string angazovaniRadnici) : Nalog(nalogodavac, sadrzaj),
                                               angazovaniRadnici(angazovaniRadnici) {}
        void stampaj() {
            Nalog::stampaj();
            cout << angazovaniRadnici << endl;
        }
    };
} // namespace nalog