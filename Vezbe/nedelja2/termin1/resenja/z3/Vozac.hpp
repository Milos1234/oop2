#ifndef VOZAC_HPP
#define VOZAC_HPP

#include "Radnik.hpp"

#include <iostream>
#include <string>

class Vozac : public Radnik
{
private:
    std::string kategorija;

public:
    Vozac();
    Vozac(std::string ime, std::string prezime,
          double plata, double osnovicaOsiguranja, std::string kategorija);
    double brutoPlata();
};

#endif