#ifndef RADNIK_HPP
#define RADNIK_HPP

#include <iostream>
#include <string>

class Radnik
{
private:
    std::string ime;
    std::string prezime;
    double plata;
    double osnovicaOsiguranja;

public:
    Radnik();
    Radnik(std::string ime, std::string prezime,
           double plata, double osnovicaOsiguranja);
    virtual double brutoPlata();
};

#endif