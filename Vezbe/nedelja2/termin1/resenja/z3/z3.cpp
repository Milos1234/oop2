/**
 * Dinamički instancirati jednog radnika i jednog vozača.
 * Ispraviti greške tako da su zadovoljeni uslovi testa.
 * Premestiti deklaracije kalsa u odvojena zaglavalja a
 * implementacije u odvojene implementacione datoteke.
 * 
 * Razdvojiti klase u odvojene datoteke.
 */

#include <iostream>
#include <string>

#include "Radnik.hpp"
#include "Vozac.hpp"

int main()
{
    //Test ispravnosti resenja.
    Radnik *radnik1 = new Radnik("Radnik", "Radnik", 10000, 0.1);
    Vozac *vozac1 = new Vozac("Vozac", "Vozac", 10000, 0.1, "C");
    if (std::abs(radnik1->brutoPlata() - (10000 + 10000 * 0.1)) < 0.1e-10 &&
        std::abs(vozac1->brutoPlata() - (10000 + 10000 * 0.1 + 10000)) < 0.1e-10)
    {
        std::cout << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.
    return 0;
}