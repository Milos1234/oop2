#include "Arhiva.hpp"

Arhiva::Arhiva()
{
}

void Arhiva::dodajDokument(Dokument &dokument)
{
    arhiviraniDokumenti.push_back(dokument);
}

Dokument Arhiva::pronadjiDokument(std::string naslov)
{
    for (size_t i = 0; i < arhiviraniDokumenti.size(); i++)
    {
        if (arhiviraniDokumenti[i].getNaslov() == naslov)
        {
            return arhiviraniDokumenti[i];
        }
    }
    return Dokument();
}

std::vector<Dokument> Arhiva::pronadjiDokumente(std::string naslov)
{
    std::vector<Dokument> pronadjeniDokumenti;
    for (size_t i = 0; i < arhiviraniDokumenti.size(); i++)
    {
        if (arhiviraniDokumenti[i].getNaslov().find(naslov) != std::string::npos)
        {
            pronadjeniDokumenti.push_back(arhiviraniDokumenti[i]);
        }
    }
    return pronadjeniDokumenti;
}