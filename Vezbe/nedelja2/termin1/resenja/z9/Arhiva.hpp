#ifndef ARHIVA_HPP
#define ARHIVA_HPP

#include <iostream>
#include <string>
#include <vector>

#include "Dokument.hpp"

class Arhiva
{
private:
    std::vector<Dokument> arhiviraniDokumenti;

public:
    Arhiva();
    void dodajDokument(Dokument &dokument);
    Dokument pronadjiDokument(std::string naslov);
    std::vector<Dokument> pronadjiDokumente(std::string naslov);
};
#endif