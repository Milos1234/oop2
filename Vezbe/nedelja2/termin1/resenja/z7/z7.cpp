/**
 * Napisati klasu Sastojak koja sadrži atribute naziv i količina.
 * Pored podrazumevnaog konstruktora i konstruktora sa parametrima i get i set metoda
 * ova klasa sadrži i metodu detalji koja ispisuje vrednosti svih atributa.
 * Metode i konstruktori su javno vidljivi.
 * 
 * Prepraviti klasu PrehrambeniProizvod tako da sadrži vektor sastojaka.
 * Dodati metodu za dodavanje sastojaka.
 * Prepraviti metodu za ispis tako da se ispod naziva i opisa ispšu
 * svi sastojci i njihova procentualna zastupljenost u proizvodu.
 */

#include <iostream>
#include "Sastojak.hpp"
#include "PrehrambeniProizvod.hpp"

int main()
{
    //Test ispravnosti resenja.
    Sastojak s1("Brasno", 50);
    Sastojak s2("Voda", 50);
    Sastojak s3("So", 1.5);
    PrehrambeniProizvod proizvod("Naziv", "Opis", 100.0);
    proizvod.dodajSastojak(s1);
    proizvod.dodajSastojak(s2);
    proizvod.dodajSastojak(s3);
    proizvod.detalji();
    //Kraj testa.
    return 0;
}