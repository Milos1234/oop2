#ifndef PREHRAMBENI_PROIZVOD_HPP
#define PREHRAMBENI_PROIZVOD_HPP

#include <iostream>
#include <string>
#include <vector>

#include "Sastojak.hpp"

class PrehrambeniProizvod
{
protected:
    std::string naziv;
    std::string opis;
    double cena;
    std::vector<Sastojak> sastojci;

public:
    PrehrambeniProizvod();
    PrehrambeniProizvod(std::string naziv, std::string opis, double cena);
    std::string getNaziv();
    void setNaziv(std::string naziv);
    std::string getOpis();
    void setOpis(std::string opis);
    double getCena();
    void setCena(double cena);
    void dodajSastojak(Sastojak &sastojak);
    void detalji();
};

#endif