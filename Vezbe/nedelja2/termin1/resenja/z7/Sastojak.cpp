#include "Sastojak.hpp"

Sastojak::Sastojak() {}
Sastojak::Sastojak(string naziv, double kolicina) : naziv(naziv), kolicina(kolicina) {}
string Sastojak::getNaziv()
{
    return naziv;
}
void Sastojak::setNaziv(string naziv)
{
    this->naziv = naziv;
}
double Sastojak::getKolicina()
{
    return kolicina;
}
void Sastojak::setKolicina(double kolicina)
{
    this->kolicina = kolicina;
}
void Sastojak::detalji()
{
    cout << naziv << ", " << kolicina << endl;
}