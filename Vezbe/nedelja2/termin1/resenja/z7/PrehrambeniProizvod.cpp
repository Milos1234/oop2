#include "PrehrambeniProizvod.hpp"

PrehrambeniProizvod::PrehrambeniProizvod() {}
PrehrambeniProizvod::PrehrambeniProizvod(std::string naziv, std::string opis, double cena) : naziv(naziv), opis(opis), cena(cena) {}
std::string PrehrambeniProizvod::getNaziv()
{
    return naziv;
}
void PrehrambeniProizvod::setNaziv(std::string naziv)
{
    this->naziv = naziv;
}
std::string PrehrambeniProizvod::getOpis()
{
    return opis;
}
void PrehrambeniProizvod::setOpis(std::string opis)
{
    this->opis = opis;
}
double PrehrambeniProizvod::getCena()
{
    return cena;
}
void PrehrambeniProizvod::setCena(double cena)
{
    this->cena = cena;
}
void PrehrambeniProizvod::dodajSastojak(Sastojak &sastojak)
{
    sastojci.push_back(sastojak);
}
void PrehrambeniProizvod::detalji()
{
    std::cout << naziv << " " << opis << ", " << cena << std::endl;
    double ukupno = 0;
    for (size_t i = 0; i < sastojci.size(); i++)
    {
        ukupno += sastojci[i].getKolicina();
    }
    for (size_t i = 0; i < sastojci.size(); i++)
    {
        std::cout << (i + 1) << ".\t" << sastojci[i].getNaziv() << " (" << 100 * sastojci[i].getKolicina() / ukupno << "%)" << std::endl;
    }
}