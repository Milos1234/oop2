#include "DirektorProizvodnje.hpp"

DirektorProizvodnje::DirektorProizvodnje() : Direktor()
{
}

DirektorProizvodnje::DirektorProizvodnje(string ime, string prezime,
                                         double plata,
                                         string preduzece,
                                         string pogon) : Direktor(ime, prezime, plata, preduzece),
                                                         pogon(pogon)
{
}

void DirektorProizvodnje::detalji()
{
    Direktor::detalji();
    cout << pogon << endl;
}