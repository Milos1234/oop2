#ifndef DIREKTOR_HPP
#define DIREKTOR_HPP

#include <iostream>
#include <string>

#include "Radnik.hpp"

using namespace std;

class Direktor : public Radnik
{
private:
    string preduzece;

protected:
public:
    Direktor();
    Direktor(string ime, string prezime, double plata, string preduzece);
    virtual void detalji();
};

#endif