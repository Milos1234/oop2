/**
 * Napisati klasu Direktor koja nasleđuje klasu radnik i uvodi
 * atribut preduzeće, ovaj atribut je tipa string. Klasa direktor
 * redefiniše metodu detalji iz klase radnik tako što dodatno ispisuje
 * naziv preduzeća.
 * 
 * Napisati klasu DirektorProizvodnje koja nasleđuje klasu Direktor
 * i uvodi atribut pogon. Ova klasa redefiniše metodu detalji tako
 * što dodatni ispisuje naziv pogona.
 * 
 * Za svaku od klasa napisati zaglavlje i implementacionu datoteku.
 */

#include <iostream>
#include <string>

#include "Direktor.hpp"
#include "DirektorProizvodnje.hpp"


int main()
{
    //Test ispravnosti resenja.
    Direktor direktor = Direktor("Direktor1", "Direktor1", 10000, "Preduzece");
    DirektorProizvodnje direktorProizvodnje = DirektorProizvodnje("Direktor2", "Direktor2", 10000, "Preduzece2", "Odeljenje");
    Radnik &r1 = direktor;
    Radnik &r2 = direktorProizvodnje;
    Direktor &r3 = direktorProizvodnje;
    std::cout << "Direktor:" << std::endl;
    r1.detalji();
    std::cout << std::endl
              << "Direktor proizvodnje:" << std::endl;
    r2.detalji();
    std::cout << std::endl
              << "Direktor proizvodnje:" << std::endl;
    r3.detalji();
    //Kraj testa.
    return 0;
}