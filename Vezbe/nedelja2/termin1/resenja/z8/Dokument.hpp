#ifndef DOKUMENT_HPP
#define DOKUMENT_HPP

#include <iostream>
#include <string>

class Dokument
{
protected:
    std::string naslov;
    std::string autor;

public:
    Dokument();
    Dokument(std::string naslov, std::string autor);
    std::string getNaslov();
    void stampaj();
};

#endif