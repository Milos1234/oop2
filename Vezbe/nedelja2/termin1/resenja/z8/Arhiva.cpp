#include "Arhiva.hpp"

Arhiva::Arhiva()
{
}

void Arhiva::dodajDokument(Dokument &dokument)
{
    arhiviraniDokumenti.push_back(dokument);
}

Dokument Arhiva::pronadjiDokument(std::string naslov)
{
    for (size_t i = 0; i < arhiviraniDokumenti.size(); i++)
    {
        if (arhiviraniDokumenti[i].getNaslov() == naslov)
        {
            return arhiviraniDokumenti[i];
        }
    }
    return Dokument();
}