#include "Radnik.hpp"

Radnik::Radnik() {}
Radnik::Radnik(std::string ime, std::string prezime,
               double plata, double osnovicaOsiguranja,
               Radnik *nadredjeni) : ime(ime), prezime(prezime),
                                     plata(plata), osnovicaOsiguranja(osnovicaOsiguranja),
                                     nadredjeni(nadredjeni)
{
}

double Radnik::brutoPlata()
{
    return plata + (plata * osnovicaOsiguranja);
}

int Radnik::posaljiIzvestaj(std::string izvestaj)
{
    Radnik *trenutni = this;
    int korak = -1;
    while (trenutni != nullptr)
    {
        if (trenutni->nadredjeni == nullptr)
        {
            std::cout << "Poruku procitao " << trenutni->ime << " " << trenutni->prezime << std::endl;
            std::cout << izvestaj << std::endl;
        }
        korak++;
        trenutni = trenutni->nadredjeni;
    }
    return korak;
}