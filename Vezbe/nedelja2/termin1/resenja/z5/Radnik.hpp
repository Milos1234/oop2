#ifndef RADNIK_HPP
#define RADNIK_HPP

#include <iostream>
#include <string>

class Radnik
{
private:
    std::string ime;
    std::string prezime;
    double plata;
    double osnovicaOsiguranja;
    Radnik *nadredjeni;

public:
    Radnik();
    Radnik(std::string ime, std::string prezime,
           double plata, double osnovicaOsiguranja, Radnik *nadredjeni=nullptr);
    virtual double brutoPlata();
    virtual int posaljiIzvestaj(std::string izvestaj);
};

#endif