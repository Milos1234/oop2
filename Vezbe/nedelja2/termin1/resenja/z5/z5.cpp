/**
 * U klasu radnik dodati atribut nadređeni koji je pokazivač tipa Radnik.
 * Dodati metodu posaljiIzvestaj(izvestaj) koja prosleđuje izveštaj
 * prvom nadređenom ukoliko ga radnik ima. Ukoliko radnik nema
 * nadređenog vrši se ispisvanje izveštaja i broj koraka koji
 * je bio potreban da poruka stigne do kranjeng radnika.
 */

#include <iostream>

#include "Radnik.hpp"

int main()
{
    //Test ispravnosti resenja.
    Radnik *r1 = new Radnik("Test1", "Test1", 10000, 0.1);
    Radnik *r2 = new Radnik("Test2", "Test2", 10000, 0.1, r1);
    Radnik *r3 = new Radnik("Test3", "Test3", 10000, 0.1, r2);
    Radnik *r4 = new Radnik("Test4", "Test4", 10000, 0.1, r3);
    int ispravnihKoraka = 0;
    if (r4->posaljiIzvestaj("Test izvestaj 1") == 3)
    {
        ispravnihKoraka++;
    };
    if (r2->posaljiIzvestaj("Test izvestaj 2") == 1)
    {
        ispravnihKoraka++;
    }
    if (r1->posaljiIzvestaj("Test izvestaj 3") == 0)
    {
        ispravnihKoraka++;
    }

    if (ispravnihKoraka == 3)
    {
        std::cout << "Zadatak je ispravno resen!" << std::endl;
    }
    //Kraj testa.
    return 0;
}