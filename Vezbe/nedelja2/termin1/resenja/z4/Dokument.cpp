#include "Dokument.hpp"

Dokument::Dokument()
{
}
Dokument::Dokument(std::string naslov, std::string autor) : naslov(naslov), autor(autor)
{
}
std::string Dokument::getNaslov()
{
    return naslov;
}
void Dokument::setNaslov(std::string naslov)
{
    this->naslov = naslov;
}
std::string Dokument::getAutor()
{
    return autor;
}
void Dokument::setAutor(std::string autor)
{
    this->autor = autor;
}
void Dokument::stampaj()
{
    std::cout << naslov << " " << autor << std::endl;
}