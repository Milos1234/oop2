#include "PravniDokument.hpp"

PravniDokument::PravniDokument() : Dokument()
{
}

PravniDokument::PravniDokument(string naslov, string autor,
                               Dokument *preambula) : Dokument(naslov, autor),
                                                     preambula(preambula)
{
}

void PravniDokument::stampaj() {
    preambula->stampaj();
    Dokument::stampaj();
}