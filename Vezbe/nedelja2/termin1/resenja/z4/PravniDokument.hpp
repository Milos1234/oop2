#ifndef PRAVNI_DOKUMENT_HPP
#define PRAVNI_DOKUMENT_HPP

#include <iostream>
#include <string>

#include "Dokument.hpp"

using namespace std;

class PravniDokument : public Dokument
{
private:
    Dokument *preambula;
protected:
public:
    PravniDokument();
    PravniDokument(string naslov, string autor, Dokument *preambula);
    virtual void stampaj();
};

#endif