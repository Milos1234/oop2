#ifndef ISPLATA_HPP
#define ISPLATA_HPP

#include "Transakcija.hpp"

class Isplata : public Transakcija
{
protected:
    string primalac;

public:
    Isplata();
    Isplata(string svrhaUplate, string pozivNaBroj, string primalac, double iznos);
    string getPrimalac();
    void setPrimalac(string primalac);
    void detalji();
};

#endif