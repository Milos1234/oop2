#include "Isplata.hpp"

Isplata::Isplata() {}
Isplata::Isplata(string svrhaUplate,
                 string pozivNaBroj,
                 string primalac,
                 double iznos) : Transakcija(svrhaUplate, pozivNaBroj, iznos),
                                    primalac(primalac)
{
}
string Isplata::getPrimalac()
{
    return primalac;
}
void Isplata::setPrimalac(string primalac)
{
    this->primalac = primalac;
}
void Isplata::detalji()
{
    cout << "Isplata " << primalac << " ";
    Transakcija::detalji();
}