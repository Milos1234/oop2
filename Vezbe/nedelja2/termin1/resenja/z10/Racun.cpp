#include "Racun.hpp"

Racun::Racun() {}
Racun::Racun(double stanje) : stanje(stanje) {}
void Racun::izvrsiUplatu(Uplata *uplata)
{
    stanje += uplata->getIznos();
    tranasakcije.push_back(uplata);
}
void Racun::izvrsiIsplatu(Isplata *isplata)
{
    stanje -= isplata->getIznos();
    tranasakcije.push_back(isplata);
}

void Racun::izvestaj()
{
    for (Transakcija *t : tranasakcije)
    {
        t->detalji();
    }
}