#ifndef TRANSAKCIJA_HPP
#define TRANSAKCIJA_HPP

#include <iostream>
#include <string>

using namespace std;

class Transakcija
{
protected:
    string svrhaUplate;
    string pozivNaBroj;
    double iznos;

public:
    Transakcija();
    Transakcija(string svrhaUplate, string pozivNaBroj, double iznos);
    string getSvrhaUplate();
    void setSvrhaUplate(const string &svrhaUplate);
    string getPozivNaBroj();
    void setPozivNaBroj(const string &pozivNaBroj);
    double getIznos();
    void setIznos(double iznos);
    virtual void detalji();
};

#endif