#include "Uplata.hpp"

Uplata::Uplata(){

};
Uplata::Uplata(string svrhaUplate,
               string pozivNaBroj,
               string uplatilac,
               double iznos) : Transakcija(svrhaUplate, pozivNaBroj, iznos),
                                   uplatilac(uplatilac)
{
}
string Uplata::getUplatilac()
{
    return uplatilac;
}
void Uplata::setUplatilac(string uplatilac)
{
    this->uplatilac = uplatilac;
}

void Uplata::detalji()
{
    cout << "Uplata " << uplatilac << " ";
    Transakcija::detalji();
}