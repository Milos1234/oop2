/**
 * Napisati apstraktnu klasu Vozilo koja sadrži atribute
 * model i proizvođač. Od metoda ova klasa sadrži podrazumevani
 * konstruktor, konstruktor sa parametrima, get i set metode i
 * metodu detalji koja ispisuje podatke o vozilu.
 * 
 * Napisati klasu Automobil koja nasleđuje klasu Vozilo i uvodi
 * atribut tip karoserije. Ova klasa redefiniše metodu detalji
 * tako što dodatno ispisuje podatke o tipu karoserije.
 * 
 * Napraviti interfejs KolekcijaVozila koji ima metode za dobavljanje
 * broja elemenata u kolekciji i dodavanje i uklanjanje elemenata.
 * Elementi su instance klase Vozilo.
 * 
 * Napraviti klasu VozniPark koja implementira interfejs KolekcijaVozila
 * i u sebi sadrži niz vozila. Niz može biti proizvoljne dužine.
 * U niz se mogu dodavati novi elementi i uklanjati postojeći.
 * Klasa VozniPark ima podrazumevani konstruktor, konstruktor
 * sa parametrima metode za dodavanje i uklanjanje vozila,
 * metodu za ispis svih vozila i metodu za dobavljanje broja
 * vozila u voznom parku.
 * 
 * Pri rešavanju zadatka ne koristiti klasu vektor.
 * Napisati testove koji potvrđuju ispravnost implementacije.
 */