/**
 * Napraviti program za upravljanje magacinom.
 * U magacinu se skladišti roba. Roba je opisana
 * zapreminom ambalaže, jedinicom mere, količinom,
 * cenom po jedinici mere i nazivom. Roba može biti
 * prehrambena roba, kućna hemija, tekstilna roba i
 * tehnička roba. Prehrambena roba ima datum isteka
 * roka trajanja, kućna hemija ima podataka o nameni,
 * tekstilna roba ima podataka o veličini, tehnička 
 * roba ima podatak o kategoriji. U klasi koja opisuje
 * magacin omogućiti dodavanje, uklanjanje i ispis
 * podataka o uskladištenoj robi. Prilikom ispisa
 * podataka o robi prvo ispisati sve podatke o
 * magacinu kao i podataka o ukupnoj vrednosti robe
 * u magacinu, potom tabelarno ispisati podatke
 * o svakoj pojedinačnoj stavci u magacinu. Pre svake
 * stavke ispisati redni broj. Od atributa magacin
 * poseduje podatak o maksimalnoj zapremini koju je
 * moguće zauzeti u magacinu kao i o trenutnoj
 * zauzetosti magacina. Onemogućiti da se u magacinu
 * nađe više robe nego što je moguće. Omogućiti
 * skladištenje i isčitavanja svih podataka o magacinu
 * i robi u magacinu u datoteku. Datoteka treba da bude
 * u formatu:
 * [Magacin]
 * kapacitet=100
 * zauzetost=100
 * [Roba]
 * tip=prehrambena
 * naziv=Zamrznuti grasak
 * zapremina=20
 * jedinicaMere=kg
 * kolicina=20
 * cena=100
 * tip=tekstil
 * naziv=Pantalone
 * zapremina=20
 * jedinicaMere=komad
 * kolicina=200
 * cena=3500
 * velicina=XL
 * tip=tehnika
 * naziv=TV
 * zapremina=60
 * jedinicaMere=komad
 * kolicina=100
 * cena=55000
 * kategorija=TV
 * tip=hemija
 * naziv=Sapun
 * jedinicaMere=komad
 * kolicina=2000
 * cena=100
 * namena=Licna higijena
 * 
 * Ispis i čitanje iz tokova realizovati preko
 * operatora za pisanje i čitanje.
 * Testirati rešenje.
 */