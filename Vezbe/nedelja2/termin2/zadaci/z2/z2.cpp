/**
 * Proširiti prethodni zadatak tako da se uvedu ograničenja
 * za pristup, brisanje i dodavanje podataka.
 * Optimizovati kolekciju tako da se proširivanje niza
 * vrši samo po potrebi, odnosno uvesti promenljivu kapacitet
 * koja definiše veličinu niza i promenljivu brojElemenata
 * koja definiše popunjenost niza. Kada brojElemenata dostigne
 * kapacitet kopirati postojeći niz u novi niz duplo većeg
 * kapaciteta. Prepraviti uklanjanje elemenata tako da se
 * elementi pomeraju u levo bez prethodnog kreiranja novog niza.
 */

