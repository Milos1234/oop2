/**
 * Proširiti prethodni program tako da se omogući
 * upravljanje radnim zadacima zaposlenih.
 * Svakom tipu zaposlenog dodati šifru zaposlenig i kolekciju
 * radnih zadataka. Priliko zapisivanja ili učitavanja
 * zaposlenih zapisati, odnosno, učitati njihove radne zadatke.
 * Svaki radni zadatak opisan je opisom i prioritetom.
 */