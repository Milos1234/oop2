/**
 * Proširiti prethodni zadatak uvođenjem sledećih funkcionalnosti:
 * Spajanje dva niza operatorom +.
 * Omogućiti ispis kolekcije na izlazni tok operatorom <<.
 * Omogućiti učitavanje kolekcije sa ulaznog toka operatorom >>.
 * Prepraviti zadatak tako de se može učitati i zapisati bilo
 * koji tip vozila. Omogućiti prilikom učitavanja podataka
 * o voznom parku da nazivi proizvođača, modela i tipa
 * karoserije sadrže razmake.
 */