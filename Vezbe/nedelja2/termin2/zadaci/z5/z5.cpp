/**
 * Napisati program za upravljanje spiskom zaposlenih.
 * Zaposleni mogu biti računovođe, prodavci i dostavljači.
 * Svi zaposleni imaju podatke o imenu i prezimenu, računovođe
 * imaju podataka o licenci, prodavci imaju podataka o šifri kase na
 * kojoj rade, vozači imaju podataka o kategoriji vozila koje voze.
 * Napisati klasu SpisakZaposlenih koja ima metode za dodavanje,
 * uklanjanje i ispis svih zaposlenih. Za skladištenje zaposlenih
 * koristiti klasu vector. U klasi SpisakZaposlenih definisati
 * operatore za ispisivanje na izlazni tok i čitanje sa ulaznog
 * toka. Testirati rešenje popunjavanjem spisak zaposlenih i
 * zapisivanjem sadržaja u datoteku a potom učitavanjem
 * sadržaja iz nastale datoteke.
 */