/**
 * Napisati apstraktnu klasu Vozilo koja sadrži atribute
 * model i proizvođač. Od metoda ova klasa sadrži podrazumevani
 * konstruktor, konstruktor sa parametrima, get i set metode i
 * metodu detalji koja ispisuje podatke o vozilu.
 * 
 * Napisati klasu Automobil koja nasleđuje klasu Vozilo i uvodi
 * atribut tip karoserije. Ova klasa redefiniše metodu detalji
 * tako što dodatno ispisuje podatke o tipu karoserije.
 * 
 * Napraviti interfejs KolekcijaVozila koji ima metode za dobavljanje
 * broja elemenata u kolekciji i dodavanje i uklanjanje elemenata.
 * Elementi su instance klase Vozilo.
 * 
 * Napraviti klasu VozniPark koja implementira interfejs KolekcijaVozila
 * i u sebi sadrži niz vozila. Niz može biti proizvoljne dužine.
 * U niz se mogu dodavati novi elementi i uklanjati postojeći.
 * Klasa VozniPark ima podrazumevani konstruktor, konstruktor
 * sa parametrima metode za dodavanje i uklanjanje vozila,
 * metodu za ispis svih vozila i metodu za dobavljanje broja
 * vozila u voznom parku.
 * 
 * Pri rešavanju zadatka ne koristiti klasu vektor.
 * Napisati testove koji potvrđuju ispravnost implementacije.
 */

#include <iostream>

#include "Vozilo.hpp"
#include "Automobil.hpp"
#include "VozniPark.hpp"

using namespace std;

int main()
{
    VozniPark *vp = new VozniPark();
    vp->dodajVozilo(new Automobil("Proizvodjac1", "Model1", "Karoserija1"));
    vp->dodajVozilo(new Automobil("Proizvodjac2", "Model2", "Karoserija2"));
    vp->dodajVozilo(new Automobil("Proizvodjac3", "Model3", "Karoserija3"));
    vp->dodajVozilo(new Automobil("Proizvodjac4", "Model4", "Karoserija4"));
    vp->ukloniVozilo(0);
    vp->detalji();
    delete vp;
    return 0;
}