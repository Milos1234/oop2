#ifndef KOLEKCIJA_VOZILA_HPP
#define KOLEKCIJA_VOZILA_HPP

#include "Vozilo.hpp"

class KolekcijaVozila
{
public:
    virtual int brojVozila() = 0;
    virtual void dodajVozilo(Vozilo *vozilo) = 0;
    virtual void ukloniVozilo(int indeks) = 0;
    virtual Vozilo* dobavi(int indeks) = 0;
    virtual ~KolekcijaVozila() = 0;
};

#endif