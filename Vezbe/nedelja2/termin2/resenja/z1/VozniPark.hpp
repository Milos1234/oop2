#ifndef VOZNI_PARK_HPP
#define VOZNI_PARK_HPP

#include <iostream>

#include "KolekcijaVozila.hpp"
#include "Vozilo.hpp"

using namespace std;

class VozniPark : public KolekcijaVozila
{
private:
    Vozilo **niz;
    int brojElemenata;

public:
    VozniPark();
    virtual int brojVozila();
    virtual void dodajVozilo(Vozilo *vozilo);
    virtual void ukloniVozilo(int indeks);
    virtual Vozilo* dobavi(int indeks);
    void detalji();
    virtual ~VozniPark();
};

#endif