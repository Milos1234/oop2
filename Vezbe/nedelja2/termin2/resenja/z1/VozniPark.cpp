#include "VozniPark.hpp"

VozniPark::VozniPark() : KolekcijaVozila(), brojElemenata(0)
{
    this->niz = new Vozilo *[brojElemenata];
}
int VozniPark::brojVozila()
{
    return brojElemenata;
}
void VozniPark::dodajVozilo(Vozilo *vozilo)
{
    Vozilo **noviNiz = new Vozilo *[brojElemenata + 1];
    for (int i = 0; i < brojElemenata; i++)
    {
        noviNiz[i] = niz[i];
    }
    noviNiz[brojElemenata] = vozilo;
    brojElemenata++;
    delete [] niz;
    niz = noviNiz;
}
void VozniPark::ukloniVozilo(int indeks)
{
    if(brojElemenata == 0) {
        return;
    }
    Vozilo **noviNiz = new Vozilo *[brojElemenata - 1];
    for (int i = 0; i < indeks; i++)
    {
        noviNiz[i] = niz[i];
    }
    for (int i = indeks + 1; i <= brojElemenata - 1; i++)
    {
        noviNiz[i - 1] = niz[i];
    }
    brojElemenata--;
    delete [] niz;
    niz = noviNiz;
}
Vozilo *VozniPark::dobavi(int indeks)
{
    return niz[indeks];
}

void VozniPark::detalji()
{
    for (int i = 0; i < brojVozila(); i++)
    {
        dobavi(i)->detalji();
        cout << "----------" << endl;
    }
}

VozniPark::~VozniPark() {
    for(int i = 0; i < brojElemenata; i++) {
        delete niz[i];
    }
    delete [] niz;
}