#ifndef AUTOMOBIL_HPP
#define AUTOMOBIL_HPP

#include "Vozilo.hpp"

class Automobil : public Vozilo
{
private:
    string tipKaroserije;

public:
    Automobil();
    Automobil(string proizvodjac, string model, string tipKaroserije);
    virtual void detalji();
    virtual ~Automobil();
};

#endif