#include "VozniPark.hpp"

VozniPark::VozniPark() : KolekcijaVozila(), kapacitet(1), brojElemenata(0)
{
    this->niz = new Vozilo *[kapacitet];
}
int VozniPark::brojVozila()
{
    return brojElemenata;
}
void VozniPark::dodajVozilo(Vozilo *vozilo)
{
    if (brojElemenata >= kapacitet)
    {
        kapacitet = kapacitet * 2;
        Vozilo **noviNiz = new Vozilo *[kapacitet];
        for (int i = 0; i < brojElemenata; i++)
        {
            noviNiz[i] = niz[i];
        }
        delete [] niz;
        niz = noviNiz;
    }
    niz[brojElemenata] = vozilo;
    brojElemenata++;
}

void VozniPark::ukloniVozilo(int indeks)
{
    if (brojElemenata == 0 || indeks < 0 || indeks >= brojElemenata)
    {
        return;
    }
    for (int i = indeks + 1; i < brojElemenata; i++)
    {
        niz[i - 1] = niz[i];
    }
    brojElemenata--;
}

Vozilo *VozniPark::dobavi(int indeks)
{
    if (indeks < 0 || indeks >= brojElemenata)
    {
        return nullptr;
    }
    return niz[indeks];
}

void VozniPark::detalji()
{
    for (int i = 0; i < brojVozila(); i++)
    {
        dobavi(i)->detalji();
        cout << "----------" << endl;
    }
}

VozniPark::~VozniPark()
{
    for (int i = 0; i < brojElemenata; i++)
    {
        delete niz[i];
    }
    delete[] niz;
}