#include "Vozilo.hpp"

Vozilo::Vozilo()
{
}
Vozilo::Vozilo(string proizvodjac, string model) : proizvodjac(proizvodjac), model(model)
{
}
string Vozilo::getProizvodjac()
{
    return proizvodjac;
}
void Vozilo::setProizvodjac(string proizvodjac)
{
    this->proizvodjac = proizvodjac;
}
string Vozilo::getModel()
{
    return model;
}
void Vozilo::setModel(string model)
{
    this->model = model;
}
void Vozilo::detalji()
{
    cout << proizvodjac << " " << model << endl;
}
Vozilo::~Vozilo()
{
}