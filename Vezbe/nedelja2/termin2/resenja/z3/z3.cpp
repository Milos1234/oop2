/**
 * Proširiti prethodni zadatak tako da se omogući skladištenje
 * proizvoljnog tipa objekata u kolekciju.
 * Definisati operator za indeksiranje i omogućiti da se
 * indeksiranje radi u nazad.
 */

#include <iostream>
#include <vector>

#include "Vozilo.hpp"
#include "Automobil.hpp"
#include "Niz.hpp"

using namespace std;

typedef Niz<Vozilo *> VozniPark;

template <>
void detalji<VozniPark>(const VozniPark &vp)
{
    for (int i = 0; i < vp.velicina(); i++)
    {
        vp[i]->detalji();
    }
}

int main()
{
    vector<Vozilo *> v;
    VozniPark vp;
    vp.dodaj(new Automobil("Proizvodjac1", "Model1", "Karoserija1"));
    vp.dodaj(new Automobil("Proizvodjac2", "Model2", "Karoserija2"));
    vp.dodaj(new Automobil("Proizvodjac3", "Model3", "Karoserija3"));
    vp.dodaj(new Automobil("Proizvodjac4", "Model4", "Karoserija4"));
    vp.dodaj(new Automobil("Proizvodjac3", "Model3", "Karoserija3"));
    vp.dodaj(new Automobil("Proizvodjac4", "Model4", "Karoserija4"));
    vp.ukloni(10);
    vp.ukloni(3);
    detalji(vp);
    return 0;
}