#ifndef NIZ_HPP
#define NIZ_HPP

#include "Kolekcija.hpp"

template <typename T>
void detalji(T const &objekat);

template <typename T>
class Niz : public Kolekcija<T>
{
private:
    T *niz;
    int kapacitet;
    int brojElemenata;

public:
    Niz();
    virtual int velicina() const;
    virtual void dodaj(T vrednost);
    virtual void ukloni(int indeks);
    virtual T dobavi(int indeks) const;
    virtual T operator[](int indeks) const;
    virtual ~Niz();

    friend void detalji<Niz<T>>(Niz<T> const &niz);
};

template <typename T>
Niz<T>::Niz() : Kolekcija<T>(), kapacitet(10), brojElemenata(0)
{
    this->niz = new T[kapacitet];
}

template <typename T>
int Niz<T>::velicina() const
{
    return brojElemenata;
}

template <typename T>
void Niz<T>::dodaj(T objekat)
{
    if (brojElemenata >= kapacitet)
    {
        kapacitet = kapacitet * 2;
        T *noviNiz = new T[kapacitet];
        for (int i = 0; i < brojElemenata; i++)
        {
            noviNiz[i] = niz[i];
        }
        delete[] niz;
        niz = noviNiz;
    }
    niz[brojElemenata] = objekat;
    brojElemenata++;
}

template <typename T>
void Niz<T>::ukloni(int indeks)
{
    if (indeks < 0 || indeks >= brojElemenata)
    {
        return;
    }
    for (int i = indeks + 1; i < brojElemenata; i++)
    {
        niz[i - 1] = niz[i];
    }
    brojElemenata--;
}

template <typename T>
T Niz<T>::dobavi(int indeks) const
{
    if (indeks < 0 || indeks >= brojElemenata)
    {
        throw out_of_range("Ne postoji element niza na zadatom indeksu.");
    }
    return niz[indeks];
}

template <typename T>
T Niz<T>::operator[](int indeks) const
{
    return dobavi(indeks);
}

template <typename T>
Niz<T>::~Niz()
{
    delete[] niz;
}

#endif